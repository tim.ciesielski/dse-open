`timescale 1 ns/ 1 ps
module PE_tb(
);


reg [3:0] q_w;
reg [3:0] q_a;
reg clk;
reg reset;
reg [0:3][7:0] weights;
reg [0:3][7:0] activations;
reg [19:0] results;

PE PE1 (
	.clk(clk),
	.reset(reset),
	.q_w(q_w),
	.q_a(q_a),
	.weights(weights),
	.activations(activations),
	.results(results)
);


task Reset;
begin
	#1 reset=1;
	#1 reset=0;
	$display("Reset");
end
endtask

initial
begin
reset=0;
clk=0;
q_w=4'b1000;
q_a=4'b1000;
weights=8'b00000001;
activations=8'b00000001;
$display("Running TB");

#1 reset=1;
#1 reset=0;
$display("Reset");



#0 weights[3]=8'b00000000;
#0 weights[2]=8'b01010101;
#0 weights[1]=8'b10101010;
#0 weights[0]=8'b11111111;
#0 activations[3]=8'b00000000;
#0 activations[2]=8'b01010101;
#0 activations[1]=8'b10101010;
#0 activations[0]=8'b11111111;

#20 Reset();
#0 q_a=4'b0010;

#0 q_w=4'b0010;

#20 Reset();
#0 q_w=4'b0100;

#20 Reset();
#0 q_w=4'b1000;

#20 Reset();
#0 q_a=4'b0100;

#0 q_w=4'b0010;

#20 Reset();
#0 q_w=4'b0100;

#20 Reset();
#0 q_w=4'b1000;

#20 Reset();
#0 q_a=4'b1000;

#0 q_w=4'b0010;

#20 Reset();
#0 q_w=4'b0100;

#20 Reset();
#0 q_w=4'b1000;


$display("End of Test");


end

always 
begin
#1 clk=~clk;
end




endmodule