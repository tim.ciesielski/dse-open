//DC ST 
module PE_DC_SA
(
	input wire [3:0] q_w,
	input wire [3:0] q_a,
	input wire clk,
	input wire reset,
	input wire [7:0] weights,
	input wire [7:0] activations,
	
	output reg [127:0] results
);

wire [3:0] qa_w, qa_a;
wire [3:0] op1 [15:0];
//4 bit zwischenergebnisse durchnummeriert
//
//0  1  2  3
//4  5  6  7
//8  9  10 11
//12 13 14 15

reg [7:0] op2 [3:0];
//
//0  1
//2  3
//


reg [15:0] op3;
//endergebnis



//always @ (posedge clk, posedge reset) begin
//
//if (reset == 1) begin
//	qa_w<=4'b1000;
//	qa_a<=4'b1000;
//end else begin
//	case (q_w)
//		4'b0001, 4'b0010 							: qa_w = 4'b0010;
//		4'b0011, 4'b0100 							: qa_w = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_w = 4'b1000;
//		default 										: qa_w = 4'b1000;
//	endcase
//	case (q_a)
//		4'b0001, 4'b0010 							: qa_a = 4'b0010;
//		4'b0011, 4'b0100 							: qa_a = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_a = 4'b1000;
//		default 										: qa_a = 4'b1000;
//	endcase
//end
//end

//aufrunden auf nächstes bit 3->4 6->8
assign qa_a = (q_a<4'b0011)? 4'b0010 : ((q_a<4'b0111)? 4'b0100 : 4'b1000);
assign qa_w = (q_w<4'b0011)? 4'b0010 : ((q_w<4'b0111)? 4'b0100 : 4'b1000);

wire [2:0] shift1 [2:0];
//wire [11:0] shift1;

//0  -  1  -
//2  3  4  5
//6  -  7  -
//8  9  10 11

wire shift_w, shift_a;


//1  -
//2  3
assign shift1[0] = (shift_a)? 3'b010 : 3'b000;
assign shift1[1] = (shift_a && shift_w)? 3'b100 : (shift_a || shift_w)? 3'b010 : 3'b000;
assign shift1[2] = (shift_w)? 3'b010 : 3'b000;
wire [3:0] shift2 [2:0];

assign shift2[0] = (qa_a==4'b1000)? 4'b0100 : 4'b0000;
assign shift2[1] = ((qa_a==4'b1000)&&(qa_w==4'b1000))? 4'b1000 : ((qa_a==4'b1000)||(qa_w==4'b1000))? 4'b0100 : 4'b0000;
assign shift2[2] = (qa_w==4'b1000)? 4'b0100 : 4'b0000;

assign shift_a = ((qa_a==4)||(qa_a==8));
assign shift_w = ((qa_w==4)||(qa_w==8));


assign  op1[0]  = weights[1:0]*activations[7:6];
assign  op1[1]  = weights[1:0]*activations[5:4]; 
assign  op1[2]  = weights[1:0]*activations[3:2]; 
assign  op1[3]  = weights[1:0]*activations[1:0];
assign  op1[4]  = weights[3:2]*activations[7:6];
assign  op1[5]  = weights[3:2]*activations[5:4]; 
assign  op1[6]  = weights[3:2]*activations[3:2]; 
assign  op1[7]  = weights[3:2]*activations[1:0];  
assign  op1[8]  = weights[5:4]*activations[7:6];
assign  op1[9]  = weights[5:4]*activations[5:4]; 
assign  op1[10] = weights[5:4]*activations[3:2]; 
assign  op1[11] = weights[5:4]*activations[1:0];
assign  op1[12] = weights[7:6]*activations[7:6];
assign  op1[13] = weights[7:6]*activations[5:4]; 
assign  op1[14] = weights[7:6]*activations[3:2]; 
assign  op1[15] = weights[7:6]*activations[1:0];  

assign op2[0] = (op1[0]<<2) 	+ op1[1]	 + (op1[4]<<4) + (op1[5]<<2);
assign op2[1] = (op1[2]<<2)	+ op1[3] + (op1[6]<<4) + (op1[7]<<2);
assign op2[2] = (op1[8]<<2) 	+ op1[9] + (op1[12]<<4) + (op1[13]<<2);
assign op2[3] = (op1[10]<<2) 	+ op1[11] + (op1[14]<<4) + (op1[15]<<2);

//assign op3 = op2[0]<<shift2[0] + op2[1] + op2[2]<<shift2[1] + op2[3]<<shift2[2];



always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 
		results<=0;
		
	end else begin

		case (qa_a)
			4'b1000 : begin
			if (qa_w==4'b1000) begin
				results[127:108] <= results[127:108] + ((op2[0]<<4) + op2[1] + (op2[2]<<8) + (op2[3]<<4));
			end else if (qa_w==4'b0100) begin
				//4x2
				results[127:118]<=results[127:120]	+	(op1[0] 			+ (op1[2]<<2));
				results[117:108]<=results[127:120]	+	(op1[1]	 		+ (op1[3]<<2));
				results[107:98]<=results[127:120]	+	(op1[4]			+ (op1[6]<<2));
				results[97:88]<=results[127:120]		+	(op1[5]	 		+ (op1[7]<<2));
				results[87:78]<=results[127:120]		+	(op1[8]	 		+ (op1[10]<<2));
				results[77:68]<=results[127:120]		+	(op1[9] 			+ (op1[11]<<2));
				results[67:58]<=results[127:120]		+	(op1[12] 		+ (op1[14]<<2));
				results[57:48]<=results[127:120]		+	(op1[13] 		+ (op1[15]<<2));
			end else begin
				//8x2
				results[127:114] 	<= results[127:114] 	+ ((op1[3] + (op1[2]<<2))+(op1[1]+(op1[0]<<2)<<4));
				results[113:110] 	<= results[113:110] 	+ ((op1[7] + (op1[6]<<2))+(op1[5]+(op1[4]<<2)<<4));
				results[99:86] 	<= results[99:86] 	+ ((op1[11] + (op1[10]<<2))+(op1[9]+(op1[8]<<2)<<4));
				results[85:72] 	<= results[85:72] 	+ ((op1[15] + (op1[14]<<2))+(op1[13]+(op1[12]<<2)<<4));
			end
			end
			4'b0100 : begin 
			if (qa_w==4'b1000) begin
				results[127:112]<=results[127:112] 	+ (op2[0] + (op2[2]<<4));
				results[111:96] <=results[111:96]  	+ (op2[1] + (op2[3]<<4));
			end else if (qa_w==4'b0100) begin
				
				results[127:120] <= results[127:120] + op2[0];
				results[119:112] <= results[119:112] + op2[1];
				results[111:104] <= results[111:104] + op2[2];
				results[103:96] <= results[103:96] + op2[3];
				
			end else begin
				//4x2
				results[127:118]<=results[127:120]	+	((op1[0]<<2) 		+ op1[1]);
				results[117:108]<=results[127:120]	+	((op1[2]<<2)	 	+ op1[3]);
				results[107:98]<=results[127:120]	+	((op1[4]<<2)		+ op1[5]);
				results[97:88]<=results[127:120]		+	((op1[6]<<2)	 	+ op1[7]);
				results[87:78]<=results[127:120]		+	((op1[8]<<2)	 	+ op1[9]);
				results[77:68]<=results[127:120]		+	((op1[10]<<2) 		+ op1[11]);
				results[67:58]<=results[127:120]		+	((op1[12]<<2) 		+ op1[13]);
				results[57:48]<=results[127:120]		+	((op1[14]<<2) 		+ op1[15]);
			end
			end
			4'b0010 : begin 
				if (qa_w==4'b1000) begin
				
					results[127:114] 	<= results[127:114] 	+ ((op1[0] + (op1[4]<<2))+(op1[8]+(op1[12]<<2)<<4));
					results[113:110] 	<= results[113:110] 	+ ((op1[1] + (op1[5]<<2))+(op1[9]+(op1[13]<<2)<<4));
					results[99:86] 	<= results[99:86] 	+ ((op1[2] + (op1[6]<<2))+(op1[10]+(op1[14]<<2)<<4));
					results[85:72] 	<= results[85:72] 	+ ((op1[3] + (op1[7]<<2))+(op1[11]+(op1[15]<<2)<<4));
				
				end else if (qa_w==4'b0100) begin
					results[127:112]<=results[127:112] 	+ ((op2[0]<<4) + (op2[1]));
					results[111:96] <=results[111:96]  	+ ((op2[2]<<4) + (op2[3]));
				end else begin
					results <= results + {4'b0000, op1[0], 4'b0000, op1[1], 4'b0000, op1[2], 4'b0000, op1[3], 4'b0000, op1[4], 4'b0000, op1[5], 4'b0000, op1[6], 4'b0000, op1[7], 4'b0000, op1[8], 4'b0000, op1[9], 4'b0000, op1[10], 4'b0000, op1[11], 4'b0000, op1[12], 4'b0000, op1[13], 4'b0000, op1[14], 4'b0000, op1[15]};
				end
			end
		endcase
	end
end

//2 takte verzögerung aber throughput 1 ergebnis pro takt

endmodule
