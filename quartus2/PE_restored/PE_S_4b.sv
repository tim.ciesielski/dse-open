//DC ST 
module PE_S_4b
(
	input wire [3:0] q_w,
	input wire [3:0] q_a,
	input wire clk,
	input wire reset,
	input wire [0:1][7:0] weights,
	input wire [0:1][7:0] activations,
	
	output wire [19:0] results
);

wire [3:0] qa_w, qa_a;
wire [7:0] op2 [0:1][0:1];
//4 bit zwischenergebnisse durchnummeriert
//
//0  1  2  3
//4  5  6  7
//8  9  10 11
//12 13 14 15

//endergebnis



//always @ (posedge clk, posedge reset) begin
//
//if (reset == 1) begin
//	qa_w<=4'b1000;
//	qa_a<=4'b1000;
//end else begin
//	case (q_w)
//		4'b0001, 4'b0010 							: qa_w = 4'b0010;
//		4'b0011, 4'b0100 							: qa_w = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_w = 4'b1000;
//		default 										: qa_w = 4'b1000;
//	endcase
//	case (q_a)
//		4'b0001, 4'b0010 							: qa_a = 4'b0010;
//		4'b0011, 4'b0100 							: qa_a = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_a = 4'b1000;
//		default 										: qa_a = 4'b1000;
//	endcase
//end
//end

//aufrunden auf nächstes bit 3->4 6->8
assign qa_a = ((q_a<4'b0111)? 4'b0100 : 4'b1000);
assign qa_w = ((q_w<4'b0111)? 4'b0100 : 4'b1000);

wire [0:1][7:0]row, column;

assign column[0]=weights[0];
assign column[1]=weights[1];


assign row[0]=activations[0];
assign row[1]=activations[1];


//assign column[0]=weights[0];
//assign column[1]=(qa_a==4) ? weights[1] : weights[0];
//
//
//assign row[0]=activations[0];
//assign row[1]=(qa_w==4) ? activations[1] : activations[0];


assign op2[0][0] = column[0][3:0]*row[0][7:4];
assign op2[1][0] = column[1][3:0]*row[0][3:0];

assign op2[0][1] = column[0][7:4]*row[1][7:4];
assign op2[1][1] = column[1][7:4]*row[1][3:0];




assign op3 = op2[0][0] + op2[0][1] + op2[1][0] + op2[1][1];

//2 takte verzögerung aber throughput 1 ergebnis pro takt

endmodule
