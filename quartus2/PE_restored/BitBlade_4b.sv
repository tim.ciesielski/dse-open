module BitBlade_4b
(
	input wire [3:0] q_w,
	input wire [3:0] q_a,
	input wire clk,
	input wire reset,
	input wire [3:0][0:1][7:0] weights,
	input wire [3:0][0:1][7:0] activations,
	
	output reg [24:0] results
);

wire PS [3:0];

wire [3:0] shift2_waagerecht, shift2_vertikal;


assign shift2_waagerecht = (q_w==4'b1000) ? 4'b0100 : 4'b0000;
assign shift2_vertikal = (q_a==4'b1000) ? 4'b0100 : 4'b0000;

genvar i;
generate 
	for (i=0; i<=3; i=i+1) begin : PE_Array
		PE_S_4b PE_S_4b(
			.q_w(q_w),
			.q_a(q_a),
			.clk(clk),
			.reset(reset),

			.activations(activations[i]),
			//.weights(weigths[i]),
			.weights(weights[i]),
			.results(PS[i])
		);
	end
endgenerate	

always @ (posedge clk) begin
	if (reset==1) begin
		results<=0;
	end else begin
		results<=results+(PS[0]<<(shift2_vertikal))+(PS[1])+(PS[2]<<(shift2_vertikal+shift2_waagerecht))+(PS[3]<<(shift2_waagerecht));
	end
end
endmodule