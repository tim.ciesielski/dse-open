//DC ST 
module PE_S
(
	input wire [3:0] q_w,
	input wire [3:0] q_a,
	input wire clk,
	input wire reset,
	input wire [0:3][7:0] weights,
	input wire [0:3][7:0] activations,
	
	output reg [19:0] results
);

wire [3:0] qa_w, qa_a;
wire [3:0] op1 [15:0];
//4 bit zwischenergebnisse durchnummeriert
//
//0  1  2  3
//4  5  6  7
//8  9  10 11
//12 13 14 15

//endergebnis



//always @ (posedge clk, posedge reset) begin
//
//if (reset == 1) begin
//	qa_w<=4'b1000;
//	qa_a<=4'b1000;
//end else begin
//	case (q_w)
//		4'b0001, 4'b0010 							: qa_w = 4'b0010;
//		4'b0011, 4'b0100 							: qa_w = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_w = 4'b1000;
//		default 										: qa_w = 4'b1000;
//	endcase
//	case (q_a)
//		4'b0001, 4'b0010 							: qa_a = 4'b0010;
//		4'b0011, 4'b0100 							: qa_a = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_a = 4'b1000;
//		default 										: qa_a = 4'b1000;
//	endcase
//end
//end

//aufrunden auf nächstes bit 3->4 6->8
assign qa_a = (q_a<4'b0011)? 4'b0010 : ((q_a<4'b0111)? 4'b0100 : 4'b1000);
assign qa_w = (q_w<4'b0011)? 4'b0010 : ((q_w<4'b0111)? 4'b0100 : 4'b1000);

assign  op1[0] 	 = (qa_w==8&&qa_a==8) ? weights[0][1:0]*activations[0][7:6] : (qa_w==8&&qa_a==4) ? weights[0][1:0]*activations[0][7:6] : (qa_w==8&&qa_a==2) ? weights[0][1:0]*activations[0][7:6] : (qa_w==4&&qa_a==8) ? weights[0][1:0]*activations[0][7:6] : (qa_w==4&&qa_a==4) ? weights[0][1:0]*activations[0][7:6] : (qa_w==4&&qa_a==2) ? weights[0][1:0]*activations[0][7:6] : (qa_w==2&&qa_a==8) ? weights[0][1:0]*activations[0][7:6] : (qa_w==2&&qa_a==4) ? weights[0][1:0]*activations[0][7:6] : (qa_w==2&&qa_a==2) ? weights[0][1:0]*activations[0][7:6] :	4'b1001;
assign  op1[1] 	 = (qa_w==8&&qa_a==8) ? weights[0][1:0]*activations[0][5:4] : (qa_w==8&&qa_a==4) ? weights[0][1:0]*activations[0][5:4] : (qa_w==8&&qa_a==2) ? weights[1][1:0]*activations[0][5:4] : (qa_w==4&&qa_a==8) ? weights[0][1:0]*activations[0][5:4] : (qa_w==4&&qa_a==4) ? weights[0][1:0]*activations[0][5:4] : (qa_w==4&&qa_a==2) ? weights[1][1:0]*activations[0][5:4] : (qa_w==2&&qa_a==8) ? weights[0][1:0]*activations[0][5:4] : (qa_w==2&&qa_a==4) ? weights[0][1:0]*activations[0][5:4] : (qa_w==2&&qa_a==2) ? weights[1][1:0]*activations[0][5:4] : 4'b1001;
assign  op1[2] 	 = (qa_w==8&&qa_a==8) ? weights[0][1:0]*activations[0][3:2] : (qa_w==8&&qa_a==4) ? weights[1][1:0]*activations[0][3:2] : (qa_w==8&&qa_a==2) ? weights[2][1:0]*activations[0][3:2] : (qa_w==4&&qa_a==8) ? weights[0][1:0]*activations[0][3:2] : (qa_w==4&&qa_a==4) ? weights[1][1:0]*activations[0][3:2] : (qa_w==4&&qa_a==2) ? weights[2][1:0]*activations[0][3:2] : (qa_w==2&&qa_a==8) ? weights[0][1:0]*activations[0][3:2] : (qa_w==2&&qa_a==4) ? weights[1][1:0]*activations[0][3:2] : (qa_w==2&&qa_a==2) ? weights[2][1:0]*activations[0][3:2] : 4'b1001;
assign  op1[3]     = (qa_w==8&&qa_a==8) ? weights[0][1:0]*activations[0][1:0] : (qa_w==8&&qa_a==4) ? weights[1][1:0]*activations[0][1:0] : (qa_w==8&&qa_a==2) ? weights[3][1:0]*activations[0][1:0] : (qa_w==4&&qa_a==8) ? weights[0][1:0]*activations[0][1:0] : (qa_w==4&&qa_a==4) ? weights[1][1:0]*activations[0][1:0] : (qa_w==4&&qa_a==2) ? weights[3][1:0]*activations[0][1:0] : (qa_w==2&&qa_a==8) ? weights[0][1:0]*activations[0][1:0] : (qa_w==2&&qa_a==4) ? weights[1][1:0]*activations[0][1:0] : (qa_w==2&&qa_a==2) ? weights[3][1:0]*activations[0][1:0] : 4'b1001;

assign  op1[4]	  	 = (qa_w==8&&qa_a==8) ? weights[0][3:2]*activations[0][7:6] : (qa_w==8&&qa_a==4) ? weights[0][3:2]*activations[0][7:6] : (qa_w==8&&qa_a==2) ? weights[0][3:2]*activations[0][7:6] : (qa_w==4&&qa_a==8) ? weights[0][3:2]*activations[0][7:6] : (qa_w==4&&qa_a==4) ? weights[0][3:2]*activations[0][7:6] : (qa_w==4&&qa_a==2) ? weights[0][3:2]*activations[0][7:6] : (qa_w==2&&qa_a==8) ? weights[0][3:2]*activations[1][7:6] : (qa_w==2&&qa_a==4) ? weights[0][3:2]*activations[1][7:6] : (qa_w==2&&qa_a==2) ? weights[0][3:2]*activations[1][7:6] : 4'b1001;
assign  op1[5] 	 = (qa_w==8&&qa_a==8) ? weights[0][3:2]*activations[0][5:4] : (qa_w==8&&qa_a==4) ? weights[0][3:2]*activations[0][5:4] : (qa_w==8&&qa_a==2) ? weights[1][3:2]*activations[0][5:4] : (qa_w==4&&qa_a==8) ? weights[0][3:2]*activations[0][5:4] : (qa_w==4&&qa_a==4) ? weights[0][3:2]*activations[0][5:4] : (qa_w==4&&qa_a==2) ? weights[1][3:2]*activations[0][5:4] : (qa_w==2&&qa_a==8) ? weights[0][3:2]*activations[1][5:4] : (qa_w==2&&qa_a==4) ? weights[0][3:2]*activations[1][5:4] : (qa_w==2&&qa_a==2) ? weights[1][3:2]*activations[1][5:4] : 4'b1001;
assign  op1[6] 	 = (qa_w==8&&qa_a==8) ? weights[0][3:2]*activations[0][3:2] : (qa_w==8&&qa_a==4) ? weights[1][3:2]*activations[0][3:2] : (qa_w==8&&qa_a==2) ? weights[2][3:2]*activations[0][3:2] : (qa_w==4&&qa_a==8) ? weights[0][3:2]*activations[0][3:2] : (qa_w==4&&qa_a==4) ? weights[1][3:2]*activations[0][3:2] : (qa_w==4&&qa_a==2) ? weights[2][3:2]*activations[0][3:2] : (qa_w==2&&qa_a==8) ? weights[0][3:2]*activations[1][3:2] : (qa_w==2&&qa_a==4) ? weights[1][3:2]*activations[1][3:2] : (qa_w==2&&qa_a==2) ? weights[2][3:2]*activations[1][3:2] : 4'b1001;
assign  op1[7] 	 = (qa_w==8&&qa_a==8) ? weights[0][3:2]*activations[0][1:0] : (qa_w==8&&qa_a==4) ? weights[1][3:2]*activations[0][1:0] : (qa_w==8&&qa_a==2) ? weights[3][3:2]*activations[0][1:0] : (qa_w==4&&qa_a==8) ? weights[0][3:2]*activations[0][1:0] : (qa_w==4&&qa_a==4) ? weights[1][3:2]*activations[0][1:0] : (qa_w==4&&qa_a==2) ? weights[3][3:2]*activations[0][1:0] : (qa_w==2&&qa_a==8) ? weights[0][3:2]*activations[1][1:0] : (qa_w==2&&qa_a==4) ? weights[1][3:2]*activations[1][1:0] : (qa_w==2&&qa_a==2) ? weights[3][3:2]*activations[1][1:0] : 4'b1001;

assign  op1[8] 	 = (qa_w==8&&qa_a==8) ? weights[0][5:4]*activations[0][7:6] : (qa_w==8&&qa_a==4) ? weights[0][5:4]*activations[0][7:6] : (qa_w==8&&qa_a==2) ? weights[0][5:4]*activations[0][7:6] : (qa_w==4&&qa_a==8) ? weights[0][5:4]*activations[1][7:6] : (qa_w==4&&qa_a==4) ? weights[0][5:4]*activations[1][7:6] : (qa_w==4&&qa_a==2) ? weights[0][5:4]*activations[1][7:6] : (qa_w==2&&qa_a==8) ? weights[0][5:4]*activations[2][7:6] : (qa_w==2&&qa_a==4) ? weights[0][5:4]*activations[2][7:6] : (qa_w==2&&qa_a==2) ? weights[0][5:4]*activations[2][7:6] : 4'b1001;
assign  op1[9] 	 = (qa_w==8&&qa_a==8) ? weights[0][5:4]*activations[0][5:4] : (qa_w==8&&qa_a==4) ? weights[0][5:4]*activations[0][5:4] : (qa_w==8&&qa_a==2) ? weights[1][5:4]*activations[0][5:4] : (qa_w==4&&qa_a==8) ? weights[0][5:4]*activations[1][5:4] : (qa_w==4&&qa_a==4) ? weights[0][5:4]*activations[1][5:4] : (qa_w==4&&qa_a==2) ? weights[1][5:4]*activations[1][5:4] : (qa_w==2&&qa_a==8) ? weights[0][5:4]*activations[2][5:4] : (qa_w==2&&qa_a==4) ? weights[0][5:4]*activations[2][5:4] : (qa_w==2&&qa_a==2) ? weights[1][5:4]*activations[2][5:4] : 4'b1001;
assign  op1[10] 	 = (qa_w==8&&qa_a==8) ? weights[0][5:4]*activations[0][3:2] : (qa_w==8&&qa_a==4) ? weights[1][5:4]*activations[0][3:2] : (qa_w==8&&qa_a==2) ? weights[2][5:4]*activations[0][3:2] : (qa_w==4&&qa_a==8) ? weights[0][5:4]*activations[1][3:2] : (qa_w==4&&qa_a==4) ? weights[1][5:4]*activations[1][3:2] : (qa_w==4&&qa_a==2) ? weights[2][5:4]*activations[1][3:2] : (qa_w==2&&qa_a==8) ? weights[0][5:4]*activations[2][3:2] : (qa_w==2&&qa_a==4) ? weights[1][5:4]*activations[2][3:2] : (qa_w==2&&qa_a==2) ? weights[2][5:4]*activations[2][3:2] : 4'b1001;
assign  op1[11] 	 = (qa_w==8&&qa_a==8) ? weights[0][5:4]*activations[0][1:0] : (qa_w==8&&qa_a==4) ? weights[1][5:4]*activations[0][1:0] : (qa_w==8&&qa_a==2) ? weights[3][5:4]*activations[0][1:0] : (qa_w==4&&qa_a==8) ? weights[0][5:4]*activations[1][1:0] : (qa_w==4&&qa_a==4) ? weights[1][5:4]*activations[1][1:0] : (qa_w==4&&qa_a==2) ? weights[3][5:4]*activations[1][1:0] : (qa_w==2&&qa_a==8) ? weights[0][5:4]*activations[2][1:0] : (qa_w==2&&qa_a==4) ? weights[1][5:4]*activations[2][1:0] : (qa_w==2&&qa_a==2) ? weights[3][5:4]*activations[2][1:0] : 4'b1001;

assign  op1[12] 	 = (qa_w==8&&qa_a==8) ? weights[0][7:6]*activations[0][7:6] : (qa_w==8&&qa_a==4) ? weights[0][7:6]*activations[0][7:6] : (qa_w==8&&qa_a==2) ? weights[0][7:6]*activations[0][7:6] : (qa_w==4&&qa_a==8) ? weights[0][7:6]*activations[1][7:6] : (qa_w==4&&qa_a==4) ? weights[0][7:6]*activations[1][7:6] : (qa_w==4&&qa_a==2) ? weights[0][7:6]*activations[1][7:6] : (qa_w==2&&qa_a==8) ? weights[0][7:6]*activations[3][7:6] : (qa_w==2&&qa_a==4) ? weights[0][7:6]*activations[3][7:6] : (qa_w==2&&qa_a==2) ? weights[0][7:6]*activations[3][7:6] : 4'b1001;
assign  op1[13] 	 = (qa_w==8&&qa_a==8) ? weights[0][7:6]*activations[0][5:4] : (qa_w==8&&qa_a==4) ? weights[0][7:6]*activations[0][5:4] : (qa_w==8&&qa_a==2) ? weights[1][7:6]*activations[0][5:4] : (qa_w==4&&qa_a==8) ? weights[0][7:6]*activations[1][5:4] : (qa_w==4&&qa_a==4) ? weights[0][7:6]*activations[1][5:4] : (qa_w==4&&qa_a==2) ? weights[1][7:6]*activations[1][5:4] : (qa_w==2&&qa_a==8) ? weights[0][7:6]*activations[3][5:4] : (qa_w==2&&qa_a==4) ? weights[0][7:6]*activations[3][5:4] : (qa_w==2&&qa_a==2) ? weights[1][7:6]*activations[3][5:4] : 4'b1001;
assign  op1[14] 	 = (qa_w==8&&qa_a==8) ? weights[0][7:6]*activations[0][3:2] : (qa_w==8&&qa_a==4) ? weights[1][7:6]*activations[0][3:2] : (qa_w==8&&qa_a==2) ? weights[2][7:6]*activations[0][3:2] : (qa_w==4&&qa_a==8) ? weights[0][7:6]*activations[1][3:2] : (qa_w==4&&qa_a==4) ? weights[1][7:6]*activations[1][3:2] : (qa_w==4&&qa_a==2) ? weights[2][7:6]*activations[1][3:2] : (qa_w==2&&qa_a==8) ? weights[0][7:6]*activations[3][3:2] : (qa_w==2&&qa_a==4) ? weights[1][7:6]*activations[3][3:2] : (qa_w==2&&qa_a==2) ? weights[2][7:6]*activations[3][3:2] : 4'b1001;
assign  op1[15] 	 = (qa_w==8&&qa_a==8) ? weights[0][7:6]*activations[0][1:0] : (qa_w==8&&qa_a==4) ? weights[1][7:6]*activations[0][1:0] : (qa_w==8&&qa_a==2) ? weights[3][7:6]*activations[0][1:0] : (qa_w==4&&qa_a==8) ? weights[0][7:6]*activations[1][1:0] : (qa_w==4&&qa_a==4) ? weights[1][7:6]*activations[1][1:0] : (qa_w==4&&qa_a==2) ? weights[3][7:6]*activations[1][1:0] : (qa_w==2&&qa_a==8) ? weights[0][7:6]*activations[3][1:0] : (qa_w==2&&qa_a==4) ? weights[1][7:6]*activations[3][1:0] : (qa_w==2&&qa_a==2) ? weights[3][7:6]*activations[3][1:0] : 4'b1001;






always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 

		results<=0;

	end else begin



		//op3 <= (op2[0]<<shift2[0]) + op2[1] + (op2[2]<<shift2[1]) + (op2[3]<<shift2[2]);
		results<=results + op1[0] + op1[1] + op1[2] + op1[3] + op1[4] + op1[5] + op1[6] + op1[7] + op1[8] + op1[9] + op1[10] + op1[11] + op1[12] + op1[13] + op1[14] + op1[15];
	end
end

//2 takte verzögerung aber throughput 1 ergebnis pro takt

endmodule
