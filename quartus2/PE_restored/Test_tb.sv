`timescale 1 ns/ 1 ps
module Test_tb(
);


reg clk;
reg reset;
reg [7:0] weights;
reg [7:0] activations;
reg [19:0] results;

Test Test (
	.clk(clk),
	.reset(reset),
	.weights(weights),
	.activations(activations),
	.results(results)
);

task Reset;
begin
	#1 reset=1;
	#1 reset=0;
	$display("Reset");
end
endtask

initial
begin
reset=0;
clk=0;

weights=8'b00010001;
activations=8'b00010001;
$display("Running TB");

Reset();

#10 weights=8'b00010001;
$display("Weights changed");
#10 weights=8'b00000001;
$display("Weights changed");
#2 weights=8'b00010001;
$display("Weights changed");
#2 weights=8'b00000001;
$display("Weights changed");


#4 activations=8'b00010001;
$display("Activations changed");
#2 activations=8'b00000001;
$display("Activations changed");

Reset();


#4 weights=8'b00010001;
$display("Weights changed");
#2 weights=8'b00000001;
$display("Weights changed");
#2 weights=8'b00010001;
$display("Weights changed");
#2 weights=8'b00000001;
$display("Weights changed");

Reset();
#4 weights=8'b01010101;
$display("Weights changed");
#0 activations=8'b01010101;
$display("Activations changed");

$display("End of Test");
end

always 
begin
#1 clk=~clk;
end




endmodule