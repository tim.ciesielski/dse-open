module Addition_8_8 (

	input wire clk,
	input wire reset,
	input wire [7:0] weights,
	input wire [7:0] activations,
	
	output reg [19:0] results
);

always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 

		results<=0;

	end else begin

		results<=results + (weights*activations);
		//op3 <= (op2[0]<<shift2[0]) + op2[1] + (op2[2]<<shift2[1]) + (op2[3]<<shift2[2]);
	end
end

endmodule