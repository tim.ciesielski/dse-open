module Test (
	input wire [7:0] activations,
	input wire [7:0] weights,
	input wire clk,
	input wire reset,
	
	output reg [19:0] results
);

wire [3:0] a1, a2, w1, w2;

assign a1 = activations[3:0];
assign a2 = activations[7:4];
assign w1 = weights[3:0];
assign w2 = weights[7:4];

wire [7:0] op1, op2;

assign op1 = a1*w1;
assign op2 = a2*w2;

wire [15:0] op3;

assign op3 = op1*op2;

always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 
		results <= 0;
	end else begin
		results <= results + op3;
	end
end

endmodule