module BitBlade_1b
(
	input wire [3:0] q_w,
	input wire [3:0] q_a,
	input wire clk,
	input wire reset,
	input wire [63:0][0:7][7:0] weights,
	input wire [63:0][0:7][7:0] activations,
	
	output reg [24:0] results
);

wire [19:0] PS [63:0];
wire  [1:0] shift_0h , shift_0v;
wire [2:0] shift_1h, shift_1v;
wire [3:0] shift_2h, shift_2v;

assign shift_0h 		= (q_w==4'b1000||q_w==4'b0100||q_w==4'b0010) ? 2'b01 : 2'b00;
assign shift_0V 		= (q_a==4'b1000||q_a==4'b0100||q_a==4'b0010) ? 2'b01 : 2'b00;
assign shift_1h 		= (q_w==4'b1000||q_w==4'b0100) ? 3'b010 : 3'b000;
assign shift_1v 		= (q_a==4'b1000||q_a==4'b0100) ? 3'b010 : 3'b000;
assign shift_2h		= (q_w==4'b1000) ? 4'b0100 : 4'b0000;
assign shift_2v 		= (q_a==4'b1000) ? 4'b0100 : 4'b0000;

genvar i;
generate 
	for (i=0; i<=63; i=i+1) begin : PE_Array
		PE_S_1b PE_S(
			.q_w(q_w),
			.q_a(q_a),
			.clk(clk),
			.reset(reset),

			.activations(activations[i]),
			//.weights(weigths[i]),
			.weights(weights[i]),
			.results(PS[i])
		);
	end
endgenerate	

wire [7:0] op0 [0:7][0:7];

genvar h, j;
generate
	for (h=0; h<=7; h=h+1) begin : Zeilen
		for (j=0; j<=7; j=j+1) begin : Spalten
			assign op0[j][h] = PS[h+(j*8)];
		end
	end
endgenerate

wire [11:0] op1 [0:3][0:3];

assign op1[0][0]=(op0[0][0]<<shift_0v)+(op0[0][1]<<(shift_0v+shift_0h))+(op0[1][0])+(op0[1][1]<<shift_0h);
assign op1[1][0]=(op0[2][0]<<shift_0v)+(op0[2][1]<<(shift_0v+shift_0h))+(op0[3][0])+(op0[3][1]<<shift_0h);
assign op1[2][0]=(op0[4][0]<<shift_0v)+(op0[4][1]<<(shift_0v+shift_0h))+(op0[5][0])+(op0[5][1]<<shift_0h);
assign op1[3][0]=(op0[6][0]<<shift_0v)+(op0[6][1]<<(shift_0v+shift_0h))+(op0[7][0])+(op0[7][1]<<shift_0h);

assign op1[0][1]=(op0[0][2]<<shift_0v)+(op0[0][3]<<(shift_0v+shift_0h))+(op0[1][2])+(op0[1][3]<<shift_0h);
assign op1[1][1]=(op0[2][2]<<shift_0v)+(op0[2][3]<<(shift_0v+shift_0h))+(op0[3][2])+(op0[3][3]<<shift_0h);
assign op1[2][1]=(op0[4][2]<<shift_0v)+(op0[4][3]<<(shift_0v+shift_0h))+(op0[5][2])+(op0[5][3]<<shift_0h);
assign op1[3][1]=(op0[6][2]<<shift_0v)+(op0[6][3]<<(shift_0v+shift_0h))+(op0[7][2])+(op0[7][3]<<shift_0h);

assign op1[0][2]=(op0[0][4]<<shift_0v)+(op0[0][5]<<(shift_0v+shift_0h))+(op0[1][4])+(op0[1][5]<<shift_0h);
assign op1[1][2]=(op0[2][4]<<shift_0v)+(op0[2][5]<<(shift_0v+shift_0h))+(op0[3][4])+(op0[3][5]<<shift_0h);
assign op1[2][2]=(op0[4][4]<<shift_0v)+(op0[4][5]<<(shift_0v+shift_0h))+(op0[5][4])+(op0[5][5]<<shift_0h);
assign op1[3][2]=(op0[6][4]<<shift_0v)+(op0[6][5]<<(shift_0v+shift_0h))+(op0[7][4])+(op0[7][5]<<shift_0h);

assign op1[0][3]=(op0[0][6]<<shift_0v)+(op0[0][7]<<(shift_0v+shift_0h))+(op0[1][6])+(op0[1][7]<<shift_0h);
assign op1[1][3]=(op0[2][6]<<shift_0v)+(op0[2][7]<<(shift_0v+shift_0h))+(op0[3][6])+(op0[3][7]<<shift_0h);
assign op1[2][3]=(op0[4][6]<<shift_0v)+(op0[4][7]<<(shift_0v+shift_0h))+(op0[5][6])+(op0[5][7]<<shift_0h);
assign op1[3][3]=(op0[6][6]<<shift_0v)+(op0[6][7]<<(shift_0v+shift_0h))+(op0[7][6])+(op0[7][7]<<shift_0h);

wire [19:0] op2 [0:1][0:1];

assign op2[0][0] = (op1[0][0]<<shift_1v)+(op1[0][1]<<(shift_1v+shift_1h))+(op1[1][0])+(op1[1][1]<<shift_1h);
assign op2[1][0] = (op1[2][0]<<shift_1v)+(op1[2][1]<<(shift_1v+shift_1h))+(op1[3][0])+(op1[3][1]<<shift_1h);

assign op2[0][1] = (op1[0][2]<<shift_1v)+(op1[0][3]<<(shift_1v+shift_1h))+(op1[1][2])+(op1[1][3]<<shift_1h);
assign op2[1][1] = (op1[2][2]<<shift_1v)+(op1[2][3]<<(shift_1v+shift_1h))+(op1[3][2])+(op1[3][3]<<shift_1h);


wire [23:0] op3;

assign  op3 = (op2[0][0]<<shift_2v)+(op2[0][1]<<(shift_2v+shift_2h))+(op2[1][0])+(op2[1][1]<<shift_2h);

always @ (posedge clk) begin
	if (reset==1) begin
		results<=0;
	end else begin
		results<=results+op3;
	end
end
endmodule