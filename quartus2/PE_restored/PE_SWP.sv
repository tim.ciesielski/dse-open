//SWP ST
module PE_SWP
(
	input wire [3:0] q,
	input wire clk,
	input wire reset,
	input wire [7:0] weights,
	input wire [7:0] activations,
	
	output reg [19:0] results
);
//
//reg [15:0] preResult;
//
////rounding up to bit
//wire [3:0] q_a;
//assign q_a = (q<4'b0011)? 4'b0010 : ((q<4'b0111)? 4'b0100 : 4'b1000);
//
//always @ (posedge clk, posedge reset) begin
//	if (reset == 1) begin
//		preResult<=0;
//		results<=0;
//	end else begin
//		case (q_a)
//			4'b0010 : preResult<=weights[1:0]*activations[7:6]+weights[3:2]*activations[5:4]+weights[5:4]*activations[3:2]+weights[7:6]*activations[1:0];
//			4'b0100 : preResult<=weights[3:0]*activations[7:4]+weights[7:4]*activations[3:0];
//			4'b1000 : preResult<=weights*activations;
//		endcase
//		results <= results + preResult;
//	end
//end

wire [3:0] q_a;
assign q_a = (q<4'b0011)? 4'b0010 : ((q<4'b0111)? 4'b0100 : 4'b1000);

always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin
		results<=0;
	end else begin
		case (q_a)
			4'b0010 : results <= results + weights[1:0]*activations[7:6]+weights[3:2]*activations[5:4]+weights[5:4]*activations[3:2]+weights[7:6]*activations[1:0];
			4'b0100 : results <= results + weights[3:0]*activations[7:4]+weights[7:4]*activations[3:0];
			4'b1000 : results <= results + weights*activations;
		endcase
	end
end


endmodule
//1caycle delay
//naive Implementation / why is it useful to reduce the register size in smaller quantizations, when the register is used anyways?