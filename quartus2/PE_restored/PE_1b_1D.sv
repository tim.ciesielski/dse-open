//DC ST 
module PE_1b_1D
(
	input wire [3:0] q_w,

	input wire clk,
	input wire reset,
	input wire [7:0] weights,
	input wire [0:7][7:0] activations,
	
	output reg [19:0] results
);

wire [3:0] qa_w;


assign qa_w = (q_w<4'b0010)? 4'b0001 :(q_w<4'b0011)? 4'b0010 : ((q_w<4'b0101)? 4'b0100 : 4'b1000);


wire [0:7][7:0]row;
wire [7:0]column;

assign column=weights[7:0];




assign row[0]=activations[0];
//assign row[1]=(qa_w==4'b0001) ? activations[1] : activations[0];
assign row[1]=(qa_w==4'b0001) ? activations[1] : activations[0];
assign row[2]=(qa_w==4) ? activations[0] : (qa_w==2) ? activations[1] : (qa_w==1) ? activations[2] : activations[0];
assign row[3]=(qa_w==4) ? activations[0] : (qa_w==2) ? activations[1] : (qa_w==1) ? activations[3] : activations[0];
assign row[4]=(qa_w==4) ? activations[1] : (qa_w==2) ? activations[2] : (qa_w==1) ? activations[4] : activations[0];
assign row[5]=(qa_w==4) ? activations[1] : (qa_w==2) ? activations[2] : (qa_w==1) ? activations[5] : activations[0];
assign row[6]=(qa_w==4) ? activations[1] : (qa_w==2) ? activations[3] : (qa_w==1) ? activations[6] : activations[0];
assign row[7]=(qa_w==4) ? activations[1] : (qa_w==2) ? activations[3] : (qa_w==1) ? activations[7] : activations[0];

wire  [9:0] op0 [7:0];
//column x row




assign op0[0]= column[0]*row[0][7:0];
assign op0[1]= column[1]*row[1][7:0];
assign op0[2]= column[2]*row[2][7:0];
assign op0[3]= column[3]*row[3][7:0];
assign op0[4]= column[4]*row[4][7:0];
assign op0[5]= column[5]*row[5][7:0];
assign op0[6]= column[6]*row[6][7:0];
assign op0[7]= column[7]*row[7][7:0];






wire [11:0] op1 [0:3];
wire [1:0] shift_0v, shift_0h;

assign shift_0h = (qa_w>=4'b0010) ? 2'b01 : 2'b00;
//assign shift_0C = ((qa_w>=4'b0010)&&(qa_a>=4'b0010)) ? 2'b10 : ((qa_w>=4'b0010)||(qa_a>=4'b0010)) ? 2'b01 : 2'b00;



assign op1[0]=(op0[0])+(op0[1]<<(shift_0h));


assign op1[1]=(op0[2])+(op0[3]<<(shift_0h));


assign op1[2]=(op0[4])+(op0[5]<<(shift_0h));


assign op1[3]=(op0[6])+(op0[7]<<(shift_0h));


wire [2:0] shift_1h, shift_1v;


assign shift_1h = (qa_w>=4) ? 3'b010 : 3'b000;

wire [11:0] op2[0:1];

assign op2[0] = (op1[0])+(op1[1]<<(shift_1h));


assign op2[1] = (op1[2])+(op1[3]<<(shift_1h));


wire [3:0] shift_2h, shift_2v;


assign shift_2h = (qa_w==8) ? 4'b0100 : 4'b0000;

wire [19:0] op3;

assign  op3 = (op2[0])+(op2[1]<<(shift_2h));


//assign  op1[1]  = weights[1:0]*activations[5:4]; 
//assign  op1[2]  = weights[1:0]*activations[3:2]; 
//assign  op1[3]  = weights[1:0]*activations[1:0];
//assign  op1[4]  = weights[3:2]*activations[7:6];
//assign  op1[5]  = weights[3:2]*activations[5:4]; 
//assign  op1[6]  = weights[3:2]*activations[3:2]; 
//assign  op1[7]  = weights[3:2]*activations[1:0];  
//assign  op1[8]  = weights[5:4]*activations[7:6];
//assign  op1[9]  = weights[5:4]*activations[5:4]; 
//assign  op1[10] = weights[5:4]*activations[3:2]; 
//assign  op1[11] = weights[5:4]*activations[1:0];
//assign  op1[12] = weights[7:6]*activations[7:6];
//assign  op1[13] = weights[7:6]*activations[5:4]; 
//assign  op1[14] = weights[7:6]*activations[3:2]; 
//assign  op1[15] = weights[7:6]*activations[1:0];  

//assign op2[0] = op1[0]<<shift1[0] 	+ op1[1]	 + op1[4]<<shift1[1] + op1[5]<<shift1[2];
//assign op2[1] = op1[2]<<shift1[0] 	+ op1[3] + op1[6]<<shift1[1] + op1[7]<<shift1[2];
//assign op2[2] = op1[8]<<shift1[0] 	+ op1[9] + op1[12]<<shift1[1] + op1[13]<<shift1[2];
//assign op2[3] = op1[10]<<shift1[0] 	+ op1[11] + op1[14]<<shift1[1] + op1[15]<<shift1[2];
//
//assign	op2[0] = (op1[0]<<shift1[0]) 	+ op1[1]	 + (op1[4]<<shift1[1]) + (op1[5]<<shift1[2]);
//assign	op2[1] = (op1[2]<<shift1[0]) 	+ op1[3] + (op1[6]<<shift1[1]) + (op1[7]<<shift1[2]);
//assign	op2[2] = (op1[8]<<shift1[0]) 	+ op1[9] + (op1[12]<<shift1[1]) + (op1[13]<<shift1[2]);
//assign	op2[3] = (op1[10]<<shift1[0]) 	+ op1[11] + (op1[14]<<shift1[1]) + (op1[15]<<shift1[2]);
//assign op3 = op2[0]<<shift2[0] + op2[1] + op2[2]<<shift2[1] + op2[3]<<shift2[2];




always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 

		results<=0;

	end else begin


		//op3 <= (op2[0]<<shift2[0]) + op2[1] + (op2[2]<<shift2[1]) + (op2[3]<<shift2[2]);
		results<=results + op3;
	end
end

//2 takte verzögerung aber throughput 1 ergebnis pro takt

endmodule
