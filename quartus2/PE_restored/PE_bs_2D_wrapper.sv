module PE_bs_2D_wrapper
(
	input wire [3:0] q_w,
	input wire [3:0] q_a,
	input wire clk,
	input wire reset,
	input wire [7:0] weights,
	input wire [7:0] activations,
	
	//output wire shift;
	//output wire noshift;
	output reg [19:0] results
);


wire [3:0] count_h, count_v;

always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 

		count_h<=0;
		count_v<=0;
		shift<=0;
		noshift<=0;
	end else if (count_v<q_a) begin
		noshift<=1;
		shift<=0;
		count_v<=count_v+1;
	end else if (count_h<(q_w+q_a-1)) begin
		noshift<=0;
		shift<=1;
		count_h<=count_h+1;
		if (count_h<q_w) begin
			count_v<=1;
		end else begin
			count_v<=count_h-q_w+1;
		end

	end else begin
		shift<=0;
		noshift<=0;
		count_h<=0;
		count_v<=0;
	end
end



PE_bs_2D_simple PE(
			.q_w(q_w),
			.q_a(q_a),
			.clk(clk),
			.reset(reset),
			.shift(shift),
			.noshift(noshift),
			.activations(activations[count_v]),
			//.weights(weigths[i]),
			.weights(weights[count_h-count_v]),
			.results(results)
		);


endmodule