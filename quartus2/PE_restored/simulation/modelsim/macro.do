
vsim -batch -logfile output_PE2

vlog -work work ../../PE.sv
vlog -work work ../../PE_tb.sv
vsim work.PE_tb
#do wave.do
#add wave -noupdate /PE_tb/clk
#log -r /*

add wave  \
sim:/PE_tb/PE1/q_w \
sim:/PE_tb/PE1/q_a \
sim:/PE_tb/PE1/clk \
sim:/PE_tb/PE1/reset \
sim:/PE_tb/PE1/weights \
sim:/PE_tb/PE1/activations \
sim:/PE_tb/PE1/results \
sim:/PE_tb/PE1/qa_w \
sim:/PE_tb/PE1/qa_a \
sim:/PE_tb/PE1/op1 \
sim:/PE_tb/PE1/op2 \
sim:/PE_tb/PE1/op3 \
sim:/PE_tb/PE1/shift1 \
sim:/PE_tb/PE1/shift_w \
sim:/PE_tb/PE1/shift_a \
sim:/PE_tb/PE1/shift2 \
sim:/PE_tb/PE1/t1 \
sim:/PE_tb/PE1/test \
sim:/PE_tb/PE1/test2 \
sim:/PE_tb/PE1/test3 \
sim:/PE_tb/PE1/test4 \
sim:/PE_tb/PE1/test5 \
sim:/PE_tb/PE1/test6

run 300ns
#exit