module PE_bs_2D_simple
(
	input wire [3:0] q_w,
	input wire [3:0] q_a,
	input wire clk,
	input wire reset,
	input wire weights,
	input wire activations,
	input wire shift,
	input wire noshift,
	output reg [19:0] results
);

reg [19:0] preResults;

always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 
		preResults<=0;
		results<=0;
	end else if (noshift) begin
		preResults<=(preResults)+weights*activations;

	end else if (shift) begin

		preResults<=(preResults<<1)+weights*activations;


	end else begin
		results<=results + preResults;
		preResults<=0;
	end
end

endmodule