//DC ST 
module PE_1D_ST
(
	input wire [3:0] q_w,
	input wire clk,
	input wire reset,
	input wire [7:0] weights,
	input wire [3:0][7:0] activations,
	
	output reg [19:0] results
);

wire [3:0] qa_w;
wire [3:0] op1 [15:0];
//4 bit zwischenergebnisse durchnummeriert
//
//0  1  2  3
//4  5  6  7
//8  9  10 11
//12 13 14 15

reg [7:0] op2 [1:0];
//
//0  1
//2  3
//


reg [15:0] op3;
//endergebnis



//always @ (posedge clk, posedge reset) begin
//
//if (reset == 1) begin
//	qa_w<=4'b1000;
//	qa_a<=4'b1000;
//end else begin
//	case (q_w)
//		4'b0001, 4'b0010 							: qa_w = 4'b0010;
//		4'b0011, 4'b0100 							: qa_w = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_w = 4'b1000;
//		default 										: qa_w = 4'b1000;
//	endcase
//	case (q_a)
//		4'b0001, 4'b0010 							: qa_a = 4'b0010;
//		4'b0011, 4'b0100 							: qa_a = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_a = 4'b1000;
//		default 										: qa_a = 4'b1000;
//	endcase
//end
//end

//aufrunden auf nächstes bit 3->4 6->8
assign qa_w = (q_w<4'b0011)? 4'b0010 : ((q_w<4'b0111)? 4'b0100 : 4'b1000);

wire [2:0] shift1;
//wire [11:0] shift1;

//0  -  1  -
//2  3  4  5
//6  -  7  -
//8  9  10 11

wire shift_w;


//1  -
//2  3

assign shift1 = (shift_w)? 3'b010 : 3'b000;
assign shift_w = ((qa_w==4)||(qa_w==8));

wire [3:0] shift2;


assign shift2 = (qa_w==4'b1000)? 4'b0100 : 4'b0000;



assign  op1[0]  = (qa_w==8) ? weights[7:6]*activations[0][7:0] :(qa_w==4) ? weights[7:6]*activations[0][7:0] : weights[7:6]*activations[0][7:0];
assign  op1[1]  = (qa_w==8) ? weights[5:4]*activations[0][7:0] :(qa_w==4) ? weights[5:4]*activations[0][7:0] : weights[5:4]*activations[1][7:0]; 
assign  op1[2]  = (qa_w==8) ? weights[3:2]*activations[0][7:0] :(qa_w==4) ? weights[3:2]*activations[1][7:0] : weights[3:2]*activations[2][7:0]; 
assign  op1[3]  = (qa_w==8) ? weights[1:0]*activations[0][7:0] :(qa_w==4) ? weights[1:0]*activations[1][7:0] : weights[1:0]*activations[3][7:0];

assign	op2[0] = (op1[0]<<shift1) 	+ op1[1];
assign 	op2[1] = (op1[2]<<shift1) 	+ op1[3];

//assign op2[0] = op1[0]<<shift1[0] 	+ op1[1]	 + op1[4]<<shift1[1] + op1[5]<<shift1[2];
//assign op2[1] = op1[2]<<shift1[0] 	+ op1[3] + op1[6]<<shift1[1] + op1[7]<<shift1[2];
//assign op2[2] = op1[8]<<shift1[0] 	+ op1[9] + op1[12]<<shift1[1] + op1[13]<<shift1[2];
//assign op2[3] = op1[10]<<shift1[0] 	+ op1[11] + op1[14]<<shift1[1] + op1[15]<<shift1[2];

//assign op3 = op2[0]<<shift2[0] + op2[1] + op2[2]<<shift2[1] + op2[3]<<shift2[2];



always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 

		results<=0;

	end else begin


		//op3 <= (op2[0]<<shift2[0]) + op2[1] + (op2[2]<<shift2[1]) + (op2[3]<<shift2[2]);
		results<=results + ((op2[0]<<shift2) + op2[1]);
	end
end

//2 takte verzögerung aber throughput 1 ergebnis pro takt

endmodule
