//DC ST 
module PE_S_2b
(
	input wire [3:0] q_w,
	input wire [3:0] q_a,
	input wire clk,
	input wire reset,
	input wire [0:3][7:0] weights,
	input wire [0:3][7:0] activations,
	
	output reg [19:0] results
);

wire [3:0] qa_w, qa_a;

assign qa_a = (q_a<4'b0011)? 4'b0010 : ((q_a<4'b0101)? 4'b0100 : 4'b1000);
assign qa_w = (q_w<4'b0011)? 4'b0010 : ((q_w<4'b0101)? 4'b0100 : 4'b1000);



wire [0:3][7:0]row, column;

//assign column[0]=weights[0];
//assign column[1]=(qa_a==1) ? weights[1] : weights[0];
//assign column[2]=(qa_a==4) ? weights[0] :(qa_a==2) ? weights[1] :(qa_a==1) ? weights[2] :weights[0];
//assign column[3]=(qa_a==4) ? weights[0] :(qa_a==2) ? weights[1] :(qa_a==1) ? weights[3] :weights[0];
//
//
//assign row[0]=activations[0];
//assign row[1]=(qa_w==1) ? activations[1] : activations[0];
//assign row[2]=(qa_w==4) ? activations[0] : (qa_w==2) ? activations[1] : (qa_w==1) ? activations[2] : activations[0];
//assign row[3]=(qa_w==4) ? activations[0] : (qa_w==2) ? activations[1] : (qa_w==1) ? activations[3] : activations[0];


assign column[0]=weights[0];
assign column[1]=weights[1];
assign column[2]=weights[2];
assign column[3]=weights[3];


assign row[0]=activations[0];
assign row[1]=activations[1];
assign row[2]=activations[2];
assign row[3]=activations[3];

wire [3:0] op1 [0:3][0:3];



assign op1[0][0]=column[0][1:0]*row[0][7:6];
assign op1[1][0]=column[1][1:0]*row[0][5:4];
assign op1[2][0]=column[2][1:0]*row[0][3:2];
assign op1[3][0]=column[3][1:0]*row[0][1:0];

assign op1[0][1]=column[0][3:2]*row[1][7:6];
assign op1[1][1]=column[1][3:2]*row[1][5:4];
assign op1[2][1]=column[2][3:2]*row[1][3:2];
assign op1[3][1]=column[3][3:2]*row[1][1:0];

assign op1[0][2]=column[0][5:4]*row[2][7:6];
assign op1[1][2]=column[1][5:4]*row[2][5:4];
assign op1[2][2]=column[2][5:4]*row[2][3:2];
assign op1[3][2]=column[3][5:4]*row[2][1:0];

assign op1[0][3]=column[0][7:6]*row[3][7:6];
assign op1[1][3]=column[1][7:6]*row[3][5:4];
assign op1[2][3]=column[2][7:6]*row[3][3:2];
assign op1[3][3]=column[3][7:6]*row[3][1:0];





always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 

		results<=0;

	end else begin


		//op3 <= (op2[0]<<shift2[0]) + op2[1] + (op2[2]<<shift2[1]) + (op2[3]<<shift2[2]);
		results<=results + op1[0][0] + op1[0][1] + op1[0][2] + op1[0][3] + op1[1][0] + op1[1][1] + op1[1][2] + op1[1][3] + op1[2][0] + op1[2][1] + op1[2][2] + op1[2][3] + op1[3][0] + op1[3][1] + op1[3][2] + op1[3][3];
	end
end

//2 takte verzögerung aber throughput 1 ergebnis pro takt

endmodule
