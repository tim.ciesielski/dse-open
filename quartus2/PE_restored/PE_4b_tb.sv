`timescale 1 ns/ 1 ps
module PE_4b_tb(
);


reg [3:0] q_w;
reg [3:0] q_a;
reg clk;
reg reset;
reg [0:1][7:0] weights;
reg [0:1][7:0] activations;
reg [19:0] results;

PE_4b PE1 (
	.clk(clk),
	.reset(reset),
	.q_w(q_w),
	.q_a(q_a),
	.weights(weights),
	.activations(activations),
	.results(results)
);


task Reset;
begin
	#1 reset=1;
	#1 reset=0;
	$display("Reset");
end
endtask

initial
begin
reset=0;
clk=0;
q_w=4'b1000;
q_a=4'b1000;
weights=8'b00000001;
activations=8'b00000001;
$display("Running TB");

#1 reset=1;
#1 reset=0;
$display("Reset");



#0 weights[1]=8'b00000010;
#0 weights[0]=8'b00000001;


#0 activations[1]=8'b00000011;
#0 activations[0]=8'b00000001;

#20 Reset();

#0 q_a=4'b0001;
#0 q_w=4'b0001;

#10 q_a=4'b0110;

#0 q_w=4'b0110;

#20 Reset();
#0 q_a=4'b0100;

#20 Reset();
#0 q_w=4'b0100;

#20 Reset();
#0 q_a=4'b1000;


$display("End of Test");


end

always 
begin
#1 clk=~clk;
end




endmodule