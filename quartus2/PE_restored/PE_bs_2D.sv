module PE_bs_2D
(
	input wire [3:0] q_w,
	input wire [3:0] q_a,
	input wire clk,
	input wire reset,
	input wire [7:0] weights,
	input wire [7:0] activations,
	
	output reg [19:0] results
);

wire [19:0] preResults;

wire [3:0] count_h, count_v;

always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 
		preResults<=0;
		results<=0;
		count_h<=0;
		count_v<=0;
	end else if (count_v<q_a) begin
		preResults<=(preResults)+weights[count_h-count_v]*activations[count_v];
		count_v<=count_v+1;
	end else if (count_h<(q_w+q_a-1)) begin

		preResults<=(preResults<<1)+weights[count_h-count_v]*activations[count_v];
		count_h<=count_h+1;
		if (count_h<q_w) begin
			count_v<=1;
		end else begin
			count_v<=count_h-q_w+1;
		end

	end else begin
		results<=results + preResults;
		count_h<=0;
		count_v<=0;
		preResults<=0;
	end
end

endmodule