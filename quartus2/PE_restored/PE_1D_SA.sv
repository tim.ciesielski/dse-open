//DC ST 
module PE_1D_SA
(
	input wire [3:0] q_w,
	input wire clk,
	input wire reset,
	input wire [7:0] weights,
	input wire [7:0] activations,
	
	output reg [55:0] results
);

wire [3:0] qa_w;
wire [9:0] op1 [3:0];
//4 bit zwischenergebnisse durchnummeriert
//
//0  1  2  3
//4  5  6  7
//8  9  10 11
//12 13 14 15

reg [7:0] op2 [1:0];
//
//0  1
//2  3
//


reg [15:0] op3;
//endergebnis



//always @ (posedge clk, posedge reset) begin
//
//if (reset == 1) begin
//	qa_w<=4'b1000;
//	qa_a<=4'b1000;
//end else begin
//	case (q_w)
//		4'b0001, 4'b0010 							: qa_w = 4'b0010;
//		4'b0011, 4'b0100 							: qa_w = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_w = 4'b1000;
//		default 										: qa_w = 4'b1000;
//	endcase
//	case (q_a)
//		4'b0001, 4'b0010 							: qa_a = 4'b0010;
//		4'b0011, 4'b0100 							: qa_a = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_a = 4'b1000;
//		default 										: qa_a = 4'b1000;
//	endcase
//end
//end

//aufrunden auf nächstes bit 3->4 6->8
assign qa_w = (q_w<4'b0011)? 4'b0010 : ((q_w<4'b0111)? 4'b0100 : 4'b1000);

//wire [2:0] shift1;
////wire [11:0] shift1;
//
////0  -  1  -
////2  3  4  5
////6  -  7  -
////8  9  10 11
//
//wire shift_w;
//
//
////1  -
////2  3
//
//assign shift1 = (shift_w)? 3'b010 : 3'b000;
//assign shift_w = ((qa_w==4)||(qa_w==8));
//
//wire [3:0] shift2;
//
//
//assign shift2 = (qa_w==4'b1000)? 4'b0100 : 4'b0000;



assign  op1[0]  = weights[7:6]*activations[7:0];
assign  op1[1]  = weights[5:4]*activations[7:0]; 
assign  op1[2]  = weights[3:2]*activations[7:0]; 
assign  op1[3]  = weights[1:0]*activations[7:0];


//assign op2[0] = op1[0]<<shift1[0] 	+ op1[1]	 + op1[4]<<shift1[1] + op1[5]<<shift1[2];
//assign op2[1] = op1[2]<<shift1[0] 	+ op1[3] + op1[6]<<shift1[1] + op1[7]<<shift1[2];
//assign op2[2] = op1[8]<<shift1[0] 	+ op1[9] + op1[12]<<shift1[1] + op1[13]<<shift1[2];
//assign op2[3] = op1[10]<<shift1[0] 	+ op1[11] + op1[14]<<shift1[1] + op1[15]<<shift1[2];

//assign op3 = op2[0]<<shift2[0] + op2[1] + op2[2]<<shift2[1] + op2[3]<<shift2[2];

wire [39:0] preResults;
wire [15:0] pre_8;
wire [11:0] pre_4_1, pre_4_2;
wire [9:0] pre_2_1, pre_2_2, pre_2_3, pre_2_4;

wire [19:0] re_8;
wire [15:0] re_4_1, re_4_2;
wire [13:0] re_2_1, re_2_2, re_2_3, re_2_4;

assign preResults[39:24] =pre_8;
assign preResults[39:28] =pre_4_1;
assign preResults[27:16] = pre_4_2;
assign preResults[39:30] =pre_2_1;
assign preResults[29:20] =pre_2_2;
assign preResults[19:10] =pre_2_3;
assign preResults[9:0] =pre_2_4;

assign re_8 = results[55:46];
assign re_4_1 = results[55:40];
assign re_4_2 = results[39:24];
assign re_2_1 = results[55:42];
assign re_2_2 = results[41:28];
assign re_2_3 = results[27:14];
assign re_2_4 = results[13:0];

assign pre_8 = (((op1[0]<<2) + op1[1])<<4) + (((op1[2]<<2)+op1[3]));
assign pre_4_1 = (op1[0]<<2) + op1[1];
assign pre_4_2 = (op1[2]<<2)+op1[3];
assign pre_2_1 = op1[0];
assign pre_2_2 = op1[1];
assign pre_2_3 = op1[2];
assign pre_2_4 = op1[3];




always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 

		results<=0;
//		op2[0]<=0;
//		op2[1]<=0;
		results<=0;
	end else begin
//		op2[0] <= (op1[0]<<shift1) 	+ op1[1];
//		op2[1] <= (op1[2]<<shift1) 	+ op1[3];

		//op3 <= (op2[0]<<shift2[0]) + op2[1] + (op2[2]<<shift2[1]) + (op2[3]<<shift2[2]);
		case (qa_w) 
			4'b1000 : begin
				results[55:46] <= results[55:46] + (((op1[0]<<2) + op1[1])<<4) + (((op1[2]<<2)+op1[3]));
			end
			4'b0100 : begin
				results[55:40] <= results[55:40] + (op1[0]<<2) + op1[1];
				results[39:24] <= results[39:24] + (op1[2]<<2)+op1[3];
			end
			4'b0010 : begin
				results[55:42] <= results[55:42] + op1[0];
				results[41:28] <= results[41:28] + op1[1];
				results[27:14] <= results[27:14] + op1[2];
				results[13:0] <= results[13:0] + op1[3];
			end
		endcase
//				case (qa_w) 
//			4'b1000 : begin
//				re_8 <= re_8 + pre_8;
//			end
//			4'b0100 : begin
//				re_4_1 <= re_4_1 + pre_4_1;
//				re_4_2 <= re_4_2 + pre_4_2;
//			end
//			4'b0010 : begin
//				re_2_1 <= re_2_1 + pre_2_1;
//				re_2_2 <= re_2_2 + pre_2_2;
//				re_2_3 <= re_2_3 + pre_2_3;
//				re_2_4 <= re_2_4 + pre_2_4;
//			end
//		endcase
	end
end

//2 takte verzögerung aber throughput 1 ergebnis pro takt

endmodule
