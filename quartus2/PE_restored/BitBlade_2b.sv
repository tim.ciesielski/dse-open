module BitBlade_2b
(
	input wire [3:0] q_w,
	input wire [3:0] q_a,
	input wire clk,
	input wire reset,
	input wire [15:0][0:3][7:0] weights,
	input wire [15:0][0:3][7:0] activations,
	
	output reg [24:0] results
);

wire PS [15:0];
wire [2:0] shift1_waagerecht, shift1_vertikal;
wire [3:0] shift2_waagerecht, shift2_vertikal;

assign shift1_waagerecht = (q_w==4'b1000||q_w==4'b0100) ? 3'b010 : 3'b000;
assign shift1_vertikal = (q_a==4'b1000||q_a==4'b0100) ? 3'b010 : 3'b000;
assign shift2_waagerecht = (q_w==4'b1000) ? 4'b0100 : 4'b0000;
assign shift2_vertikal = (q_a==4'b1000) ? 4'b0100 : 4'b0000;

genvar i;
generate 
	for (i=0; i<=15; i=i+1) begin : PE_Array
		PE_S_2b PE_S(
			.q_w(q_w),
			.q_a(q_a),
			.clk(clk),
			.reset(reset),

			.activations(activations[i]),
			//.weights(weigths[i]),
			.weights(weights[i]),
			.results(PS[i])
		);
	end
endgenerate	

always @ (posedge clk) begin
	if (reset==1) begin
		results<=0;
	end else begin
		results<=results+(PS[0]<<(shift1_vertikal+shift2_vertikal))+(PS[1]<<(shift2_vertikal))+(PS[2]<<(shift1_vertikal))+(PS[3])+(PS[4]<<(shift1_vertikal+shift1_waagerecht+shift2_vertikal))+(PS[5]<<(shift1_waagerecht+shift2_vertikal))+(PS[6]<<(shift1_waagerecht+shift1_vertikal))+(PS[7]<<(shift1_waagerecht))+(PS[8]<<(shift1_vertikal+shift2_waagerecht+shift2_vertikal))+(PS[9]<<(shift2_waagerecht+shift2_vertikal))+(PS[10]<<(shift1_vertikal+shift2_waagerecht))+(PS[11]<<(shift2_waagerecht))+(PS[12]<<(shift1_vertikal+shift1_waagerecht+shift2_vertikal+shift2_waagerecht))+(PS[13]<<(shift1_waagerecht+shift2_vertikal+shift2_waagerecht))+(PS[14]<<(shift1_vertikal+shift1_waagerecht+shift2_waagerecht))+(PS[15]<<(shift1_waagerecht+shift2_waagerecht));
	end
end
endmodule