module PE_bs_1D
(
	input wire [3:0] q_w,
	input wire clk,
	input wire reset,
	input wire [7:0] weights,
	input wire activations,
	
	output reg [19:0] results
);

wire [19:0] preResults;

wire [3:0] count;

always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 
		preResults<=0;
		results<=0;
		count<=0;
	end else if (count<q_w) begin

		preResults<=(preResults<<1)+weights*activations;
		count<=count+1;
		//op3 <= (op2[0]<<shift2[0]) + op2[1] + (op2[2]<<shift2[1]) + (op2[3]<<shift2[2]);
	end else begin
		results<=results + preResults;
		preResults<=0;
		count<=0;
	end
end

endmodule