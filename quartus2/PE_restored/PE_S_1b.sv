//DC ST 
module PE_S_1b
(
	input wire [3:0] q_w,
	input wire [3:0] q_a,
	input wire clk,
	input wire reset,
	input wire [0:7][7:0] weights,
	input wire [0:7][7:0] activations,
	
	output reg [19:0] results
);

wire [3:0] qa_w, qa_a;
//wire [3:0] op1 [3:0][3:0];
//4 bit zwischenergebnisse durchnummeriert
//
//0  1  2  3
//4  5  6  7
//8  9  10 11
//12 13 14 15

//reg [7:0] op2 [3:0];
//
//0  1
//2  3
//



//reg [15:0] op3;
//endergebnis



//always @ (posedge clk, posedge reset) begin
//
//if (reset == 1) begin
//	qa_w<=4'b1000;
//	qa_a<=4'b1000;
//end else begin
//	case (q_w)
//		4'b0001, 4'b0010 							: qa_w = 4'b0010;
//		4'b0011, 4'b0100 							: qa_w = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_w = 4'b1000;
//		default 										: qa_w = 4'b1000;
//	endcase
//	case (q_a)
//		4'b0001, 4'b0010 							: qa_a = 4'b0010;
//		4'b0011, 4'b0100 							: qa_a = 4'b0100;
//		4'b0101, 4'b0110, 4'b0111, 4'b1000	: qa_a = 4'b1000;
//		default 										: qa_a = 4'b1000;
//	endcase
//end
//end

//aufrunden auf nächstes bit 3->4 6->8
assign qa_a = (q_a<4'b0010)? 4'b0001 :(q_a<4'b0011)? 4'b0010 : ((q_a<4'b0101)? 4'b0100 : 4'b1000);
assign qa_w = (q_w<4'b0010)? 4'b0001 :(q_w<4'b0011)? 4'b0010 : ((q_w<4'b0101)? 4'b0100 : 4'b1000);

//wire [2:0] shift1 [2:0];
////wire [11:0] shift1;
//
////0  -  1  -
////2  3  4  5
////6  -  7  -
////8  9  10 11
//
//wire shift_w, shift_a;


//1  -
//2  3
//assign shift1[0] = (shift_a)? 3'b010 : 3'b000;
//assign shift1[1] = (shift_a && shift_w)? 3'b100 : (shift_a || shift_w)? 3'b010 : 3'b000;
//assign shift1[2] = (shift_w)? 3'b010 : 3'b000;
//wire [3:0] shift2 [2:0];
//
//assign shift2[0] = (qa_a==4'b1000)? 4'b0100 : 4'b0000;
//assign shift2[1] = ((qa_a==4'b1000)&&(qa_w==4'b1000))? 4'b1000 : ((qa_a==4'b1000)||(qa_w==4'b1000))? 4'b0100 : 4'b0000;
//assign shift2[2] = (qa_w==4'b1000)? 4'b0100 : 4'b0000;
//
//assign shift_a = ((qa_a==4)||(qa_a==8));
//assign shift_w = ((qa_w==4)||(qa_w==8));
//
//wire [3:0] t1[15:0];

//assign t1 = weights[0][1:0]*activations[0][7:6];

//assign  op1[0] 	 = (qa_w==8&&qa_a==8) ? weights[0][1:0]*activations[0][7:6] : (qa_w==8&&qa_a==4) ? weights[0][1:0]*activations[0][7:6] : (qa_w==8&&qa_a==2) ? weights[0][1:0]*activations[0][7:6] : (qa_w==4&&qa_a==8) ? weights[0][1:0]*activations[0][7:6] : (qa_w==4&&qa_a==4) ? weights[0][1:0]*activations[0][7:6] : (qa_w==4&&qa_a==2) ? weights[0][1:0]*activations[0][7:6] : (qa_w==2&&qa_a==8) ? weights[0][1:0]*activations[0][7:6] : (qa_w==2&&qa_a==4) ? weights[0][1:0]*activations[0][7:6] : (qa_w==2&&qa_a==2) ? weights[0][1:0]*activations[0][7:6] :	4'b1001;
//assign  op1[1] 	 = (qa_w==8&&qa_a==8) ? weights[0][1:0]*activations[0][5:4] : (qa_w==8&&qa_a==4) ? weights[0][1:0]*activations[0][5:4] : (qa_w==8&&qa_a==2) ? weights[1][1:0]*activations[0][5:4] : (qa_w==4&&qa_a==8) ? weights[0][1:0]*activations[0][5:4] : (qa_w==4&&qa_a==4) ? weights[0][1:0]*activations[0][5:4] : (qa_w==4&&qa_a==2) ? weights[1][1:0]*activations[0][5:4] : (qa_w==2&&qa_a==8) ? weights[0][1:0]*activations[0][5:4] : (qa_w==2&&qa_a==4) ? weights[0][1:0]*activations[0][5:4] : (qa_w==2&&qa_a==2) ? weights[1][1:0]*activations[0][5:4] : 4'b1001;
//assign  op1[2] 	 = (qa_w==8&&qa_a==8) ? weights[0][1:0]*activations[0][3:2] : (qa_w==8&&qa_a==4) ? weights[1][1:0]*activations[0][3:2] : (qa_w==8&&qa_a==2) ? weights[2][1:0]*activations[0][3:2] : (qa_w==4&&qa_a==8) ? weights[0][1:0]*activations[0][3:2] : (qa_w==4&&qa_a==4) ? weights[1][1:0]*activations[0][3:2] : (qa_w==4&&qa_a==2) ? weights[2][1:0]*activations[0][3:2] : (qa_w==2&&qa_a==8) ? weights[0][1:0]*activations[0][3:2] : (qa_w==2&&qa_a==4) ? weights[1][1:0]*activations[0][3:2] : (qa_w==2&&qa_a==2) ? weights[2][1:0]*activations[0][3:2] : 4'b1001;
//assign  op1[3]     = (qa_w==8&&qa_a==8) ? weights[0][1:0]*activations[0][1:0] : (qa_w==8&&qa_a==4) ? weights[1][1:0]*activations[0][1:0] : (qa_w==8&&qa_a==2) ? weights[3][1:0]*activations[0][1:0] : (qa_w==4&&qa_a==8) ? weights[0][1:0]*activations[0][1:0] : (qa_w==4&&qa_a==4) ? weights[1][1:0]*activations[0][1:0] : (qa_w==4&&qa_a==2) ? weights[3][1:0]*activations[0][1:0] : (qa_w==2&&qa_a==8) ? weights[0][1:0]*activations[0][1:0] : (qa_w==2&&qa_a==4) ? weights[1][1:0]*activations[0][1:0] : (qa_w==2&&qa_a==2) ? weights[3][1:0]*activations[0][1:0] : 4'b1001;
//
//assign  op1[4]	  	 = (qa_w==8&&qa_a==8) ? weights[0][3:2]*activations[0][7:6] : (qa_w==8&&qa_a==4) ? weights[0][3:2]*activations[0][7:6] : (qa_w==8&&qa_a==2) ? weights[0][3:2]*activations[0][7:6] : (qa_w==4&&qa_a==8) ? weights[0][3:2]*activations[0][7:6] : (qa_w==4&&qa_a==4) ? weights[0][3:2]*activations[0][7:6] : (qa_w==4&&qa_a==2) ? weights[0][3:2]*activations[0][7:6] : (qa_w==2&&qa_a==8) ? weights[0][3:2]*activations[1][7:6] : (qa_w==2&&qa_a==4) ? weights[0][3:2]*activations[1][7:6] : (qa_w==2&&qa_a==2) ? weights[0][3:2]*activations[1][7:6] : 4'b1001;
//assign  op1[5] 	 = (qa_w==8&&qa_a==8) ? weights[0][3:2]*activations[0][5:4] : (qa_w==8&&qa_a==4) ? weights[0][3:2]*activations[0][5:4] : (qa_w==8&&qa_a==2) ? weights[1][3:2]*activations[0][5:4] : (qa_w==4&&qa_a==8) ? weights[0][3:2]*activations[0][5:4] : (qa_w==4&&qa_a==4) ? weights[0][3:2]*activations[0][5:4] : (qa_w==4&&qa_a==2) ? weights[1][3:2]*activations[0][5:4] : (qa_w==2&&qa_a==8) ? weights[0][3:2]*activations[1][5:4] : (qa_w==2&&qa_a==4) ? weights[0][3:2]*activations[1][5:4] : (qa_w==2&&qa_a==2) ? weights[1][3:2]*activations[1][5:4] : 4'b1001;
//assign  op1[6] 	 = (qa_w==8&&qa_a==8) ? weights[0][3:2]*activations[0][3:2] : (qa_w==8&&qa_a==4) ? weights[1][3:2]*activations[0][3:2] : (qa_w==8&&qa_a==2) ? weights[2][3:2]*activations[0][3:2] : (qa_w==4&&qa_a==8) ? weights[0][3:2]*activations[0][3:2] : (qa_w==4&&qa_a==4) ? weights[1][3:2]*activations[0][3:2] : (qa_w==4&&qa_a==2) ? weights[2][3:2]*activations[0][3:2] : (qa_w==2&&qa_a==8) ? weights[0][3:2]*activations[1][3:2] : (qa_w==2&&qa_a==4) ? weights[1][3:2]*activations[1][3:2] : (qa_w==2&&qa_a==2) ? weights[2][3:2]*activations[1][3:2] : 4'b1001;
//assign  op1[7] 	 = (qa_w==8&&qa_a==8) ? weights[0][3:2]*activations[0][1:0] : (qa_w==8&&qa_a==4) ? weights[1][3:2]*activations[0][1:0] : (qa_w==8&&qa_a==2) ? weights[3][3:2]*activations[0][1:0] : (qa_w==4&&qa_a==8) ? weights[0][3:2]*activations[0][1:0] : (qa_w==4&&qa_a==4) ? weights[1][3:2]*activations[0][1:0] : (qa_w==4&&qa_a==2) ? weights[3][3:2]*activations[0][1:0] : (qa_w==2&&qa_a==8) ? weights[0][3:2]*activations[1][1:0] : (qa_w==2&&qa_a==4) ? weights[1][3:2]*activations[1][1:0] : (qa_w==2&&qa_a==2) ? weights[3][3:2]*activations[1][1:0] : 4'b1001;
//
//assign  op1[8] 	 = (qa_w==8&&qa_a==8) ? weights[0][5:4]*activations[0][7:6] : (qa_w==8&&qa_a==4) ? weights[0][5:4]*activations[0][7:6] : (qa_w==8&&qa_a==2) ? weights[0][5:4]*activations[0][7:6] : (qa_w==4&&qa_a==8) ? weights[0][5:4]*activations[1][7:6] : (qa_w==4&&qa_a==4) ? weights[0][5:4]*activations[1][7:6] : (qa_w==4&&qa_a==2) ? weights[0][5:4]*activations[1][7:6] : (qa_w==2&&qa_a==8) ? weights[0][5:4]*activations[2][7:6] : (qa_w==2&&qa_a==4) ? weights[0][5:4]*activations[2][7:6] : (qa_w==2&&qa_a==2) ? weights[0][5:4]*activations[2][7:6] : 4'b1001;
//assign  op1[9] 	 = (qa_w==8&&qa_a==8) ? weights[0][5:4]*activations[0][5:4] : (qa_w==8&&qa_a==4) ? weights[0][5:4]*activations[0][5:4] : (qa_w==8&&qa_a==2) ? weights[1][5:4]*activations[0][5:4] : (qa_w==4&&qa_a==8) ? weights[0][5:4]*activations[1][5:4] : (qa_w==4&&qa_a==4) ? weights[0][5:4]*activations[1][5:4] : (qa_w==4&&qa_a==2) ? weights[1][5:4]*activations[1][5:4] : (qa_w==2&&qa_a==8) ? weights[0][5:4]*activations[2][5:4] : (qa_w==2&&qa_a==4) ? weights[0][5:4]*activations[2][5:4] : (qa_w==2&&qa_a==2) ? weights[1][5:4]*activations[2][5:4] : 4'b1001;
//assign  op1[10] 	 = (qa_w==8&&qa_a==8) ? weights[0][5:4]*activations[0][3:2] : (qa_w==8&&qa_a==4) ? weights[1][5:4]*activations[0][3:2] : (qa_w==8&&qa_a==2) ? weights[2][5:4]*activations[0][3:2] : (qa_w==4&&qa_a==8) ? weights[0][5:4]*activations[1][3:2] : (qa_w==4&&qa_a==4) ? weights[1][5:4]*activations[1][3:2] : (qa_w==4&&qa_a==2) ? weights[2][5:4]*activations[1][3:2] : (qa_w==2&&qa_a==8) ? weights[0][5:4]*activations[2][3:2] : (qa_w==2&&qa_a==4) ? weights[1][5:4]*activations[2][3:2] : (qa_w==2&&qa_a==2) ? weights[2][5:4]*activations[2][3:2] : 4'b1001;
//assign  op1[11] 	 = (qa_w==8&&qa_a==8) ? weights[0][5:4]*activations[0][1:0] : (qa_w==8&&qa_a==4) ? weights[1][5:4]*activations[0][1:0] : (qa_w==8&&qa_a==2) ? weights[3][5:4]*activations[0][1:0] : (qa_w==4&&qa_a==8) ? weights[0][5:4]*activations[1][1:0] : (qa_w==4&&qa_a==4) ? weights[1][5:4]*activations[1][1:0] : (qa_w==4&&qa_a==2) ? weights[3][5:4]*activations[1][1:0] : (qa_w==2&&qa_a==8) ? weights[0][5:4]*activations[2][1:0] : (qa_w==2&&qa_a==4) ? weights[1][5:4]*activations[2][1:0] : (qa_w==2&&qa_a==2) ? weights[3][5:4]*activations[2][1:0] : 4'b1001;
//
//assign  op1[12] 	 = (qa_w==8&&qa_a==8) ? weights[0][7:6]*activations[0][7:6] : (qa_w==8&&qa_a==4) ? weights[0][7:6]*activations[0][7:6] : (qa_w==8&&qa_a==2) ? weights[0][7:6]*activations[0][7:6] : (qa_w==4&&qa_a==8) ? weights[0][7:6]*activations[1][7:6] : (qa_w==4&&qa_a==4) ? weights[0][7:6]*activations[1][7:6] : (qa_w==4&&qa_a==2) ? weights[0][7:6]*activations[1][7:6] : (qa_w==2&&qa_a==8) ? weights[0][7:6]*activations[3][7:6] : (qa_w==2&&qa_a==4) ? weights[0][7:6]*activations[3][7:6] : (qa_w==2&&qa_a==2) ? weights[0][7:6]*activations[3][7:6] : 4'b1001;
//assign  op1[13] 	 = (qa_w==8&&qa_a==8) ? weights[0][7:6]*activations[0][5:4] : (qa_w==8&&qa_a==4) ? weights[0][7:6]*activations[0][5:4] : (qa_w==8&&qa_a==2) ? weights[1][7:6]*activations[0][5:4] : (qa_w==4&&qa_a==8) ? weights[0][7:6]*activations[1][5:4] : (qa_w==4&&qa_a==4) ? weights[0][7:6]*activations[1][5:4] : (qa_w==4&&qa_a==2) ? weights[1][7:6]*activations[1][5:4] : (qa_w==2&&qa_a==8) ? weights[0][7:6]*activations[3][5:4] : (qa_w==2&&qa_a==4) ? weights[0][7:6]*activations[3][5:4] : (qa_w==2&&qa_a==2) ? weights[1][7:6]*activations[3][5:4] : 4'b1001;
//assign  op1[14] 	 = (qa_w==8&&qa_a==8) ? weights[0][7:6]*activations[0][3:2] : (qa_w==8&&qa_a==4) ? weights[1][7:6]*activations[0][3:2] : (qa_w==8&&qa_a==2) ? weights[2][7:6]*activations[0][3:2] : (qa_w==4&&qa_a==8) ? weights[0][7:6]*activations[1][3:2] : (qa_w==4&&qa_a==4) ? weights[1][7:6]*activations[1][3:2] : (qa_w==4&&qa_a==2) ? weights[2][7:6]*activations[1][3:2] : (qa_w==2&&qa_a==8) ? weights[0][7:6]*activations[3][3:2] : (qa_w==2&&qa_a==4) ? weights[1][7:6]*activations[3][3:2] : (qa_w==2&&qa_a==2) ? weights[2][7:6]*activations[3][3:2] : 4'b1001;
//assign  op1[15] 	 = (qa_w==8&&qa_a==8) ? weights[0][7:6]*activations[0][1:0] : (qa_w==8&&qa_a==4) ? weights[1][7:6]*activations[0][1:0] : (qa_w==8&&qa_a==2) ? weights[3][7:6]*activations[0][1:0] : (qa_w==4&&qa_a==8) ? weights[0][7:6]*activations[1][1:0] : (qa_w==4&&qa_a==4) ? weights[1][7:6]*activations[1][1:0] : (qa_w==4&&qa_a==2) ? weights[3][7:6]*activations[1][1:0] : (qa_w==2&&qa_a==8) ? weights[0][7:6]*activations[3][1:0] : (qa_w==2&&qa_a==4) ? weights[1][7:6]*activations[3][1:0] : (qa_w==2&&qa_a==2) ? weights[3][7:6]*activations[3][1:0] : 4'b1001;



wire [0:7][7:0]row, column;

assign column[0]=weights[0];
assign column[1]=(qa_a==1) ? weights[1] : weights[0];
assign column[2]=(qa_a==4) ? weights[0] :(qa_a==2) ? weights[1] :(qa_a==1) ? weights[2] :weights[0];
assign column[3]=(qa_a==4) ? weights[0] :(qa_a==2) ? weights[1] :(qa_a==1) ? weights[3] :weights[0];
assign column[4]=(qa_a==4) ? weights[1] :(qa_a==2) ? weights[2] :(qa_a==1) ? weights[4] :weights[0];
assign column[5]=(qa_a==4) ? weights[1] :(qa_a==2) ? weights[2] :(qa_a==1) ? weights[5] :weights[0];
assign column[6]=(qa_a==4) ? weights[1] :(qa_a==2) ? weights[3] :(qa_a==1) ? weights[6] :weights[0];
assign column[7]=(qa_a==4) ? weights[1] :(qa_a==2) ? weights[3] :(qa_a==1) ? weights[7] :weights[0];

assign row[0]=activations[0];
assign row[1]=(qa_w==1) ? activations[1] : activations[0];
assign row[2]=(qa_w==4) ? activations[0] : (qa_w==2) ? activations[1] : (qa_w==1) ? activations[2] : activations[0];
assign row[3]=(qa_w==4) ? activations[0] : (qa_w==2) ? activations[1] : (qa_w==1) ? activations[3] : activations[0];
assign row[4]=(qa_w==4) ? activations[1] : (qa_w==2) ? activations[2] : (qa_w==1) ? activations[4] : activations[0];
assign row[5]=(qa_w==4) ? activations[1] : (qa_w==2) ? activations[2] : (qa_w==1) ? activations[5] : activations[0];
assign row[6]=(qa_w==4) ? activations[1] : (qa_w==2) ? activations[3] : (qa_w==1) ? activations[6] : activations[0];
assign row[7]=(qa_w==4) ? activations[1] : (qa_w==2) ? activations[3] : (qa_w==1) ? activations[7] : activations[0];

wire  op0 [7:0][0:7];
//column x row


//[1][0]
//x0
//xx
//


//assign op0[0][0]= column[0][7]*row[0][7];
//assign op0[0][1]= column[0][6]*row[1][7];
//assign op0[0][2]= column[0][5]*row[2][7];
//assign op0[0][3]= column[0][4]*row[3][7];
//assign op0[0][4]= column[0][3]*row[4][7];
//assign op0[0][5]= column[0][2]*row[5][7];
//assign op0[0][6]= column[0][1]*row[6][7];
//assign op0[0][7]= column[0][0]*row[7][7];
//
//assign op0[1][0]= column[1][7]*row[0][6];
//assign op0[1][1]= column[1][6]*row[1][6];
//assign op0[1][2]= column[1][5]*row[2][6];
//assign op0[1][3]= column[1][4]*row[3][6];
//assign op0[1][4]= column[1][3]*row[4][6];
//assign op0[1][5]= column[1][2]*row[5][6];
//assign op0[1][6]= column[1][1]*row[6][6];
//assign op0[1][7]= column[1][0]*row[7][6];
//
//assign op0[2][0]= column[2][7]*row[0][5];
//assign op0[2][1]= column[2][6]*row[1][5];
//assign op0[2][2]= column[2][5]*row[2][5];
//assign op0[2][3]= column[2][4]*row[3][5];
//assign op0[2][4]= column[2][3]*row[4][5];
//assign op0[2][5]= column[2][2]*row[5][5];
//assign op0[2][6]= column[2][1]*row[6][5];
//assign op0[2][7]= column[2][0]*row[7][5];
//
//assign op0[3][0]= column[3][7]*row[0][4];
//assign op0[3][1]= column[3][6]*row[1][4];
//assign op0[3][2]= column[3][5]*row[2][4];
//assign op0[3][3]= column[3][4]*row[3][4];
//assign op0[3][4]= column[3][3]*row[4][4];
//assign op0[3][5]= column[3][2]*row[5][4];
//assign op0[3][6]= column[3][1]*row[6][4];
//assign op0[3][7]= column[3][0]*row[7][4];
//
//assign op0[4][0]= column[4][7]*row[0][3];
//assign op0[4][1]= column[4][6]*row[1][3];
//assign op0[4][2]= column[4][5]*row[2][3];
//assign op0[4][3]= column[4][4]*row[3][3];
//assign op0[4][4]= column[4][3]*row[4][3];
//assign op0[4][5]= column[4][2]*row[5][3];
//assign op0[4][6]= column[4][1]*row[6][3];
//assign op0[4][7]= column[4][0]*row[7][3];
//
//assign op0[5][0]= column[5][7]*row[0][2];
//assign op0[5][1]= column[5][6]*row[1][2];
//assign op0[5][2]= column[5][5]*row[2][2];
//assign op0[5][3]= column[5][4]*row[3][2];
//assign op0[5][4]= column[5][3]*row[4][2];
//assign op0[5][5]= column[5][2]*row[5][2];
//assign op0[5][6]= column[5][1]*row[6][2];
//assign op0[5][7]= column[5][0]*row[7][2];
//
//assign op0[6][0]= column[6][7]*row[0][1];
//assign op0[6][1]= column[6][6]*row[1][1];
//assign op0[6][2]= column[6][5]*row[2][1];
//assign op0[6][3]= column[6][4]*row[3][1];
//assign op0[6][4]= column[6][3]*row[4][1];
//assign op0[6][5]= column[6][2]*row[5][1];
//assign op0[6][6]= column[6][1]*row[6][1];
//assign op0[6][7]= column[6][0]*row[7][1];
//
//assign op0[7][0]= column[7][7]*row[0][0];
//assign op0[7][1]= column[7][6]*row[1][0];
//assign op0[7][2]= column[7][5]*row[2][0];
//assign op0[7][3]= column[7][4]*row[3][0];
//assign op0[7][4]= column[7][3]*row[4][0];
//assign op0[7][5]= column[7][2]*row[5][0];
//assign op0[7][6]= column[7][1]*row[6][0];
//assign op0[7][7]= column[7][0]*row[7][0];

//assign op0[0][0]= column[0][7]*row[0][0];
//assign op0[0][1]= column[0][6]*row[1][0];
//assign op0[0][2]= column[0][5]*row[2][0];
//assign op0[0][3]= column[0][4]*row[3][0];
//assign op0[0][4]= column[0][3]*row[4][0];
//assign op0[0][5]= column[0][2]*row[5][0];
//assign op0[0][6]= column[0][1]*row[6][0];
//assign op0[0][7]= column[0][0]*row[7][0];
//
//assign op0[1][0]= column[1][7]*row[0][1];
//assign op0[1][1]= column[1][6]*row[1][1];
//assign op0[1][2]= column[1][5]*row[2][1];
//assign op0[1][3]= column[1][4]*row[3][1];
//assign op0[1][4]= column[1][3]*row[4][1];
//assign op0[1][5]= column[1][2]*row[5][1];
//assign op0[1][6]= column[1][1]*row[6][1];
//assign op0[1][7]= column[1][0]*row[7][1];
//
//assign op0[2][0]= column[2][7]*row[0][2];
//assign op0[2][1]= column[2][6]*row[1][2];
//assign op0[2][2]= column[2][5]*row[2][2];
//assign op0[2][3]= column[2][4]*row[3][2];
//assign op0[2][4]= column[2][3]*row[4][2];
//assign op0[2][5]= column[2][2]*row[5][2];
//assign op0[2][6]= column[2][1]*row[6][2];
//assign op0[2][7]= column[2][0]*row[7][2];
//
//assign op0[3][0]= column[3][7]*row[0][3];
//assign op0[3][1]= column[3][6]*row[1][3];
//assign op0[3][2]= column[3][5]*row[2][3];
//assign op0[3][3]= column[3][4]*row[3][3];
//assign op0[3][4]= column[3][3]*row[4][3];
//assign op0[3][5]= column[3][2]*row[5][3];
//assign op0[3][6]= column[3][1]*row[6][3];
//assign op0[3][7]= column[3][0]*row[7][3];
//
//assign op0[4][0]= column[4][7]*row[0][4];
//assign op0[4][1]= column[4][6]*row[1][4];
//assign op0[4][2]= column[4][5]*row[2][4];
//assign op0[4][3]= column[4][4]*row[3][4];
//assign op0[4][4]= column[4][3]*row[4][4];
//assign op0[4][5]= column[4][2]*row[5][4];
//assign op0[4][6]= column[4][1]*row[6][4];
//assign op0[4][7]= column[4][0]*row[7][4];
//
//assign op0[5][0]= column[5][7]*row[0][5];
//assign op0[5][1]= column[5][6]*row[1][5];
//assign op0[5][2]= column[5][5]*row[2][5];
//assign op0[5][3]= column[5][4]*row[3][5];
//assign op0[5][4]= column[5][3]*row[4][5];
//assign op0[5][5]= column[5][2]*row[5][5];
//assign op0[5][6]= column[5][1]*row[6][5];
//assign op0[5][7]= column[5][0]*row[7][5];
//
//assign op0[6][0]= column[6][7]*row[0][6];
//assign op0[6][1]= column[6][6]*row[1][6];
//assign op0[6][2]= column[6][5]*row[2][6];
//assign op0[6][3]= column[6][4]*row[3][6];
//assign op0[6][4]= column[6][3]*row[4][6];
//assign op0[6][5]= column[6][2]*row[5][6];
//assign op0[6][6]= column[6][1]*row[6][6];
//assign op0[6][7]= column[6][0]*row[7][6];
//
//assign op0[7][0]= column[7][7]*row[0][7];
//assign op0[7][1]= column[7][6]*row[1][7];
//assign op0[7][2]= column[7][5]*row[2][7];
//assign op0[7][3]= column[7][4]*row[3][7];
//assign op0[7][4]= column[7][3]*row[4][7];
//assign op0[7][5]= column[7][2]*row[5][7];
//assign op0[7][6]= column[7][1]*row[6][7];
//assign op0[7][7]= column[7][0]*row[7][7];

assign op0[0][0]= column[0][0]*row[0][7];
assign op0[0][1]= column[0][1]*row[1][7];
assign op0[0][2]= column[0][2]*row[2][7];
assign op0[0][3]= column[0][3]*row[3][7];
assign op0[0][4]= column[0][4]*row[4][7];
assign op0[0][5]= column[0][5]*row[5][7];
assign op0[0][6]= column[0][6]*row[6][7];
assign op0[0][7]= column[0][7]*row[7][7];

assign op0[1][0]= column[1][0]*row[0][6];
assign op0[1][1]= column[1][1]*row[1][6];
assign op0[1][2]= column[1][2]*row[2][6];
assign op0[1][3]= column[1][3]*row[3][6];
assign op0[1][4]= column[1][4]*row[4][6];
assign op0[1][5]= column[1][5]*row[5][6];
assign op0[1][6]= column[1][6]*row[6][6];
assign op0[1][7]= column[1][7]*row[7][6];

assign op0[2][0]= column[2][0]*row[0][5];
assign op0[2][1]= column[2][1]*row[1][5];
assign op0[2][2]= column[2][2]*row[2][5];
assign op0[2][3]= column[2][3]*row[3][5];
assign op0[2][4]= column[2][4]*row[4][5];
assign op0[2][5]= column[2][5]*row[5][5];
assign op0[2][6]= column[2][6]*row[6][5];
assign op0[2][7]= column[2][7]*row[7][5];

assign op0[3][0]= column[3][0]*row[0][4];
assign op0[3][1]= column[3][1]*row[1][4];
assign op0[3][2]= column[3][2]*row[2][4];
assign op0[3][3]= column[3][3]*row[3][4];
assign op0[3][4]= column[3][4]*row[4][4];
assign op0[3][5]= column[3][5]*row[5][4];
assign op0[3][6]= column[3][6]*row[6][4];
assign op0[3][7]= column[3][7]*row[7][4];

assign op0[4][0]= column[4][0]*row[0][3];
assign op0[4][1]= column[4][1]*row[1][3];
assign op0[4][2]= column[4][2]*row[2][3];
assign op0[4][3]= column[4][3]*row[3][3];
assign op0[4][4]= column[4][4]*row[4][3];
assign op0[4][5]= column[4][5]*row[5][3];
assign op0[4][6]= column[4][6]*row[6][3];
assign op0[4][7]= column[4][7]*row[7][3];

assign op0[5][0]= column[5][0]*row[0][2];
assign op0[5][1]= column[5][1]*row[1][2];
assign op0[5][2]= column[5][2]*row[2][2];
assign op0[5][3]= column[5][3]*row[3][2];
assign op0[5][4]= column[5][4]*row[4][2];
assign op0[5][5]= column[5][5]*row[5][2];
assign op0[5][6]= column[5][6]*row[6][2];
assign op0[5][7]= column[5][7]*row[7][2];

assign op0[6][0]= column[6][0]*row[0][1];
assign op0[6][1]= column[6][1]*row[1][1];
assign op0[6][2]= column[6][2]*row[2][1];
assign op0[6][3]= column[6][3]*row[3][1];
assign op0[6][4]= column[6][4]*row[4][1];
assign op0[6][5]= column[6][5]*row[5][1];
assign op0[6][6]= column[6][6]*row[6][1];
assign op0[6][7]= column[6][7]*row[7][1];

assign op0[7][0]= column[7][0]*row[0][0];
assign op0[7][1]= column[7][1]*row[1][0];
assign op0[7][2]= column[7][2]*row[2][0];
assign op0[7][3]= column[7][3]*row[3][0];
assign op0[7][4]= column[7][4]*row[4][0];
assign op0[7][5]= column[7][5]*row[5][0];
assign op0[7][6]= column[7][6]*row[6][0];
assign op0[7][7]= column[7][7]*row[7][0];


//assign op0[0][0]= column[0][7]*row[0][0];
//assign op0[0][1]= column[0][6]*row[1][0];
//assign op0[0][2]= column[0][5]*row[2][0];
//assign op0[0][3]= column[0][4]*row[3][0];
//assign op0[0][4]= column[0][3]*row[4][0];
//assign op0[0][5]= column[0][2]*row[5][0];
//assign op0[0][6]= column[0][1]*row[6][0];
//assign op0[0][7]= column[0][0]*row[7][0];
//
//assign op0[1][0]= column[1][7]*row[0][1];
//assign op0[1][1]= column[1][6]*row[1][1];
//assign op0[1][2]= column[1][5]*row[2][1];
//assign op0[1][3]= column[1][4]*row[3][1];
//assign op0[1][4]= column[1][3]*row[4][1];
//assign op0[1][5]= column[1][2]*row[5][1];
//assign op0[1][6]= column[1][1]*row[6][1];
//assign op0[1][7]= column[1][0]*row[7][1];
//
//assign op0[2][0]= column[2][7]*row[0][2];
//assign op0[2][1]= column[2][6]*row[1][2];
//assign op0[2][2]= column[2][5]*row[2][2];
//assign op0[2][3]= column[2][4]*row[3][2];
//assign op0[2][4]= column[2][3]*row[4][2];
//assign op0[2][5]= column[2][2]*row[5][2];
//assign op0[2][6]= column[2][1]*row[6][2];
//assign op0[2][7]= column[2][0]*row[7][2];
//
//assign op0[3][0]= column[3][7]*row[0][3];
//assign op0[3][1]= column[3][6]*row[1][3];
//assign op0[3][2]= column[3][5]*row[2][3];
//assign op0[3][3]= column[3][4]*row[3][3];
//assign op0[3][4]= column[3][3]*row[4][3];
//assign op0[3][5]= column[3][2]*row[5][3];
//assign op0[3][6]= column[3][1]*row[6][3];
//assign op0[3][7]= column[3][0]*row[7][3];
//
//assign op0[4][0]= column[4][7]*row[0][4];
//assign op0[4][1]= column[4][6]*row[1][4];
//assign op0[4][2]= column[4][5]*row[2][4];
//assign op0[4][3]= column[4][4]*row[3][4];
//assign op0[4][4]= column[4][3]*row[4][4];
//assign op0[4][5]= column[4][2]*row[5][4];
//assign op0[4][6]= column[4][1]*row[6][4];
//assign op0[4][7]= column[4][0]*row[7][4];
//
//assign op0[5][0]= column[5][7]*row[0][5];
//assign op0[5][1]= column[5][6]*row[1][5];
//assign op0[5][2]= column[5][5]*row[2][5];
//assign op0[5][3]= column[5][4]*row[3][5];
//assign op0[5][4]= column[5][3]*row[4][5];
//assign op0[5][5]= column[5][2]*row[5][5];
//assign op0[5][6]= column[5][1]*row[6][5];
//assign op0[5][7]= column[5][0]*row[7][5];
//
//assign op0[6][0]= column[6][7]*row[0][6];
//assign op0[6][1]= column[6][6]*row[1][6];
//assign op0[6][2]= column[6][5]*row[2][6];
//assign op0[6][3]= column[6][4]*row[3][6];
//assign op0[6][4]= column[6][3]*row[4][6];
//assign op0[6][5]= column[6][2]*row[5][6];
//assign op0[6][6]= column[6][1]*row[6][6];
//assign op0[6][7]= column[6][0]*row[7][6];
//
//assign op0[7][0]= column[7][7]*row[0][7];
//assign op0[7][1]= column[7][6]*row[1][7];
//assign op0[7][2]= column[7][5]*row[2][7];
//assign op0[7][3]= column[7][4]*row[3][7];
//assign op0[7][4]= column[7][3]*row[4][7];
//assign op0[7][5]= column[7][2]*row[5][7];
//assign op0[7][6]= column[7][1]*row[6][7];
//assign op0[7][7]= column[7][0]*row[7][7];

//
//
//assign op0[0][0] = row[0]*column[0];
//assign op0[0][1] = row[0]*column[1];
//assign op0[0][2] = row[0]*column[2];
//assign op0[0][3] = row[0]*column[3];
//assign op0[0][4] = row[0]*column[4];
//assign op0[0][5] = row[0]*column[5];
//assign op0[0][6] = row[0]*column[6];
//assign op0[0][7] = row[0]*column[7];
//
//assign op0[1][0] = row[1][0]*column[0][1];
//assign op0[1][1] = row[1][1]*column[1][1];
//assign op0[1][2] = row[1][2]*column[2][1];
//assign op0[1][3] = row[1][3]*column[3][1];
//assign op0[1][4] = row[1][4]*column[4][1];
//assign op0[1][5] = row[1][5]*column[5][1];
//assign op0[1][6] = row[1][6]*column[6][1];
//assign op0[1][7] = row[1][7]*column[7][1];
//
//assign op0[2][0] = row[2]*column[0];
//assign op0[2][1] = row[2]*column[1];
//assign op0[2][2] = row[2]*column[2];
//assign op0[2][3] = row[2]*column[3];
//assign op0[2][4] = row[2]*column[4];
//assign op0[2][5] = row[2]*column[5];
//assign op0[2][6] = row[2]*column[6];
//assign op0[2][7] = row[2]*column[7];
//
//assign op0[3][0] = row[3]*column[0];
//assign op0[3][1] = row[3]*column[1];
//assign op0[3][2] = row[3]*column[2];
//assign op0[3][3] = row[3]*column[3];
//assign op0[3][4] = row[3]*column[4];
//assign op0[3][5] = row[3]*column[5];
//assign op0[3][6] = row[3]*column[6];
//assign op0[3][7] = row[3]*column[7];
//
//assign op0[4][0] = row[4]*column[0];
//assign op0[4][1] = row[4]*column[1];
//assign op0[4][2] = row[4]*column[2];
//assign op0[4][3] = row[4]*column[3];
//assign op0[4][4] = row[4]*column[4];
//assign op0[4][5] = row[4]*column[5];
//assign op0[4][6] = row[4]*column[6];
//assign op0[4][7] = row[4]*column[7];
//
//assign op0[5][0] = row[5]*column[0];
//assign op0[5][1] = row[5]*column[1];
//assign op0[5][2] = row[5]*column[2];
//assign op0[5][3] = row[5]*column[3];
//assign op0[5][4] = row[5]*column[4];
//assign op0[5][5] = row[5]*column[5];
//assign op0[5][6] = row[5]*column[6];
//assign op0[5][7] = row[5]*column[7];
//
//assign op0[6][0] = row[6]*column[0];
//assign op0[6][1] = row[6]*column[1];
//assign op0[6][2] = row[6]*column[2];
//assign op0[6][3] = row[6]*column[3];
//assign op0[6][4] = row[6]*column[4];
//assign op0[6][5] = row[6]*column[5];
//assign op0[6][6] = row[6]*column[6];
//assign op0[6][7] = row[6]*column[7];
//
//assign op0[7][0] = row[7]*column[0];
//assign op0[7][1] = row[7]*column[1];
//assign op0[7][2] = row[7]*column[2];
//assign op0[7][3] = row[7]*column[3];
//assign op0[7][4] = row[7]*column[4];
//assign op0[7][5] = row[7]*column[5];
//assign op0[7][6] = row[7]*column[6];
//assign op0[7][7] = row[7]*column[7];




always @ (posedge clk, posedge reset) begin
	if (reset == 1) begin 

		results<=0;

	end else begin


		//op3 <= (op2[0]<<shift2[0]) + op2[1] + (op2[2]<<shift2[1]) + (op2[3]<<shift2[2]);
		results<=results + op0[0][0] + op0[0][1] + op0[0][2] + op0[0][3] + op0[0][4] + op0[0][5] + op0[0][6] + op0[0][7] + op0[1][0] + op0[1][1] + op0[1][2] + op0[1][3] + op0[1][4] + op0[1][5] + op0[1][6] + op0[1][7] + op0[2][0] + op0[2][1] + op0[2][2] + op0[2][3] + op0[2][4] + op0[2][5] + op0[2][6] + op0[2][7] + op0[3][0] + op0[3][1] + op0[3][2] + op0[3][3] + op0[3][4] + op0[3][5] + op0[3][6] + op0[3][7] + op0[4][0] + op0[4][1] + op0[4][2] + op0[4][3] + op0[4][4] + op0[4][5] + op0[4][6] + op0[4][7] + op0[5][0] + op0[5][1] + op0[5][2] + op0[5][3] + op0[5][4] + op0[5][5] + op0[5][6] + op0[5][7] + op0[6][0] + op0[6][1] + op0[6][2] + op0[6][3] + op0[6][4] + op0[6][5] + op0[6][6] + op0[6][7] + op0[7][0] + op0[7][1] + op0[7][2] + op0[7][3] + op0[7][4] + op0[7][5] + op0[7][6] + op0[7][7];
	end
end

//2 takte verzögerung aber throughput 1 ergebnis pro takt

endmodule
